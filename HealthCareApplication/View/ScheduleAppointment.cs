﻿using System;
using System.Windows.Forms;
using HealthCareApplication.Controller;
using HealthCareApplication.Properties;

namespace HealthCareApplication.View
{
    /// <summary>
    ///     Creates the Schedule Appointment form class
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class ScheduleAppointment : Form
    {
        #region Data members

        private readonly DoctorController docController = new DoctorController();
        private readonly PatientController patientController = new PatientController();
        private readonly AppointmentController appController = new AppointmentController();

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the patient identifier.
        /// </summary>
        /// <value>
        ///     The patient identifier.
        /// </value>
        public int PatientId { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ScheduleAppointment" /> class.
        /// </summary>
        public ScheduleAppointment()
        {
            this.InitializeComponent();
            this.fillDoctorPullDownMenuWithNames();
        }

        #endregion

        #region Methods

        private void fillDoctorPullDownMenuWithNames()
        {
            var doctors = this.docController.GetAllDoctors();
            foreach (var doctor in doctors)
            {
                this.doctorNameCombobox.Items.AddRange(new object[] {
                        doctor.Fname + " " + doctor.Lname
                    });
            }
        }

        private void scheduleAppointmentSubmitButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.doctorNameCombobox.SelectedItem == null)
                {
                    MessageBox.Show(
                        Resources
                            .ScheduleAppointment_scheduleAppointmentSubmitButton_Click_You_must_chose_a_doctor_from_drop_down_list_);
                }
                else if (this.reasonForAppointmentTextBox.Text == string.Empty)
                {
                    MessageBox.Show(
                        Resources
                            .ScheduleAppointment_scheduleAppointmentSubmitButton_Click_All_fields_must_be_filled_out_);
                }

                else
                {
                    var fullName = this.doctorNameCombobox.Text;
                    var firstName = fullName.Split(' ')[0];
                    var lastName = fullName.Split(' ')[1];
                    var doctors = this.docController.GetAllDoctors();

                    foreach (var doctor in doctors)
                    {
                        if ((doctor.Fname == firstName) && (doctor.Lname == lastName))
                        {
                            this.appController.AddAppointment(this.scheduleAppointmentDatePicker.Value,
                                    this.reasonForAppointmentTextBox.Text, null, doctor.Id, this.PatientId);
                            MessageBox.Show(
                                Resources.ScheduleAppointment_scheduleAppointmentSubmitButton_Click_Appointment_Created_);
                            Close();
                        }
                    }
                }
            }
            catch (SystemException ecp)
            {
                MessageBox.Show(string.Format(Resources.AdminForm_sqlSubmitButton_Click_An_error_occurred___0_,
                    ecp.Message));
            }
        }

        #endregion
    }
}