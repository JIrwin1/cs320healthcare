﻿using System;
using System.Windows.Forms;
using HealthCareApplication.Controller;
using HealthCareApplication.Properties;

namespace HealthCareApplication.View
{
    /// <summary>
    ///     Creates the DiagnosisForm class
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class DiagnosisForm : Form
    {
        #region Data members

        /// <summary>
        ///     The diagnosis routine check identifier
        /// </summary>
        public static int DiagnosisRoutineCheckId;

        private readonly DiagnosisController diagnosisController = new DiagnosisController();

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="DiagnosisForm" /> class.
        /// </summary>
        public DiagnosisForm()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        private void submitDiagnosisButton_Click(object sender, EventArgs e)
        {
            if (this.diagnosisRichTextBox.Text == string.Empty)
            {
                MessageBox.Show(Resources.DiagnosisForm_submitDiagnosisButton_Click_A_diagnosis_must_be_entered_);
            }
            else
            {
                try
                {
                    this.diagnosisController.AddDiagnosis(this.diagnosisRichTextBox.Text, DiagnosisRoutineCheckId,null);
                    MessageBox.Show(Resources.DiagnosisForm_submitDiagnosisButton_Click_Diagnosis_has_been_added__);
                    Close();
                }
                catch (SystemException ecp)
                {
                    MessageBox.Show(string.Format(Resources.AdminForm_sqlSubmitButton_Click_An_error_occurred___0_,
                        ecp.Message));
                }
            }
        }

        private void cancelDiagnosisButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion
    }
}