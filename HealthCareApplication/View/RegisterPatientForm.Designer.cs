﻿using System.Windows.Forms;

namespace HealthCareApplication.View
{
    partial class RegisterPatientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fNameTextBox = new System.Windows.Forms.TextBox();
            this.lNameTextBox = new System.Windows.Forms.TextBox();
            this.street1TextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.street1Label = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.registerPatientOkButton = new System.Windows.Forms.Button();
            this.registerPatientCancelButton = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.contactMaskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.street2TextBox = new System.Windows.Forms.TextBox();
            this.cityLabel = new System.Windows.Forms.Label();
            this.stateLabel = new System.Windows.Forms.Label();
            this.cityTextBox = new System.Windows.Forms.TextBox();
            this.stateTextBox = new System.Windows.Forms.TextBox();
            this.zipLabel = new System.Windows.Forms.Label();
            this.zipTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // fNameTextBox
            // 
            this.fNameTextBox.Location = new System.Drawing.Point(133, 23);
            this.fNameTextBox.Name = "fNameTextBox";
            this.fNameTextBox.Size = new System.Drawing.Size(100, 20);
            this.fNameTextBox.TabIndex = 0;
            // 
            // lNameTextBox
            // 
            this.lNameTextBox.Location = new System.Drawing.Point(133, 53);
            this.lNameTextBox.Name = "lNameTextBox";
            this.lNameTextBox.Size = new System.Drawing.Size(100, 20);
            this.lNameTextBox.TabIndex = 1;
            // 
            // street1TextBox
            // 
            this.street1TextBox.Location = new System.Drawing.Point(133, 84);
            this.street1TextBox.Name = "street1TextBox";
            this.street1TextBox.Size = new System.Drawing.Size(100, 20);
            this.street1TextBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "First Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Last Name:";
            // 
            // street1Label
            // 
            this.street1Label.AutoSize = true;
            this.street1Label.Location = new System.Drawing.Point(18, 91);
            this.street1Label.Name = "street1Label";
            this.street1Label.Size = new System.Drawing.Size(47, 13);
            this.street1Label.TabIndex = 8;
            this.street1Label.Text = "Street 1:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 253);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Date of Birth:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 289);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Contact Number:";
            
            // 
            // registerPatientOkButton
            // 
            this.registerPatientOkButton.Location = new System.Drawing.Point(158, 344);
            this.registerPatientOkButton.Name = "registerPatientOkButton";
            this.registerPatientOkButton.Size = new System.Drawing.Size(75, 23);
            this.registerPatientOkButton.TabIndex = 11;
            this.registerPatientOkButton.Text = "Enter Information";
            this.registerPatientOkButton.UseVisualStyleBackColor = true;
            this.registerPatientOkButton.Click += new System.EventHandler(this.registerPatientOkButton_Click);
            // 
            // registerPatientCancelButton
            // 
            this.registerPatientCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.registerPatientCancelButton.Location = new System.Drawing.Point(21, 344);
            this.registerPatientCancelButton.Name = "registerPatientCancelButton";
            this.registerPatientCancelButton.Size = new System.Drawing.Size(75, 23);
            this.registerPatientCancelButton.TabIndex = 12;
            this.registerPatientCancelButton.Text = "Cancel";
            this.registerPatientCancelButton.UseVisualStyleBackColor = true;
            this.registerPatientCancelButton.Click += new System.EventHandler(this.registerPatientCancelButton_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "dd-MM-yyyy";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(133, 246);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(100, 20);
            this.dateTimePicker1.TabIndex = 13;
            // 
            // contactMaskedTextBox1
            // 
            this.contactMaskedTextBox1.Location = new System.Drawing.Point(133, 282);
            this.contactMaskedTextBox1.Mask = "(000) 000-0000";
            this.contactMaskedTextBox1.Name = "contactMaskedTextBox1";
            this.contactMaskedTextBox1.Size = new System.Drawing.Size(100, 20);
            this.contactMaskedTextBox1.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Street 2:";
            // 
            // street2TextBox
            // 
            this.street2TextBox.Location = new System.Drawing.Point(133, 117);
            this.street2TextBox.Name = "street2TextBox";
            this.street2TextBox.Size = new System.Drawing.Size(100, 20);
            this.street2TextBox.TabIndex = 16;
            // 
            // cityLabel
            // 
            this.cityLabel.AutoSize = true;
            this.cityLabel.Location = new System.Drawing.Point(18, 154);
            this.cityLabel.Name = "cityLabel";
            this.cityLabel.Size = new System.Drawing.Size(27, 13);
            this.cityLabel.TabIndex = 17;
            this.cityLabel.Text = "City:";
            // 
            // stateLabel
            // 
            this.stateLabel.AutoSize = true;
            this.stateLabel.Location = new System.Drawing.Point(18, 182);
            this.stateLabel.Name = "stateLabel";
            this.stateLabel.Size = new System.Drawing.Size(35, 13);
            this.stateLabel.TabIndex = 18;
            this.stateLabel.Text = "State:";
            // 
            // cityTextBox
            // 
            this.cityTextBox.Location = new System.Drawing.Point(133, 147);
            this.cityTextBox.Name = "cityTextBox";
            this.cityTextBox.Size = new System.Drawing.Size(100, 20);
            this.cityTextBox.TabIndex = 19;
            // 
            // stateTextBox
            // 
            this.stateTextBox.Location = new System.Drawing.Point(133, 175);
            this.stateTextBox.Name = "stateTextBox";
            this.stateTextBox.Size = new System.Drawing.Size(100, 20);
            this.stateTextBox.TabIndex = 20;
            // 
            // zipLabel
            // 
            this.zipLabel.AutoSize = true;
            this.zipLabel.Location = new System.Drawing.Point(20, 216);
            this.zipLabel.Name = "zipLabel";
            this.zipLabel.Size = new System.Drawing.Size(25, 13);
            this.zipLabel.TabIndex = 21;
            this.zipLabel.Text = "Zip:";
            // 
            // zipTextBox
            // 
            this.zipTextBox.Location = new System.Drawing.Point(133, 209);
            this.zipTextBox.Name = "zipTextBox";
            this.zipTextBox.Size = new System.Drawing.Size(100, 20);
            this.zipTextBox.TabIndex = 22;
            // 
            // RegisterPatientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.registerPatientCancelButton;
            this.ClientSize = new System.Drawing.Size(244, 374);
            this.Controls.Add(this.zipTextBox);
            this.Controls.Add(this.zipLabel);
            this.Controls.Add(this.stateTextBox);
            this.Controls.Add(this.cityTextBox);
            this.Controls.Add(this.stateLabel);
            this.Controls.Add(this.cityLabel);
            this.Controls.Add(this.street2TextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.contactMaskedTextBox1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.registerPatientCancelButton);
            this.Controls.Add(this.registerPatientOkButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.street1Label);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.street1TextBox);
            this.Controls.Add(this.lNameTextBox);
            this.Controls.Add(this.fNameTextBox);
            this.Name = "RegisterPatientForm";
            this.Text = "RegisterPatientForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox fNameTextBox;
        private System.Windows.Forms.TextBox lNameTextBox;
        private System.Windows.Forms.TextBox street1TextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label street1Label;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button registerPatientOkButton;
        private System.Windows.Forms.Button registerPatientCancelButton;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private MaskedTextBox contactMaskedTextBox1;
        private Label label3;
        private TextBox street2TextBox;
        private Label cityLabel;
        private Label stateLabel;
        private TextBox cityTextBox;
        private TextBox stateTextBox;
        private Label zipLabel;
        private TextBox zipTextBox;
    }
}