﻿using System;
using System.Windows.Forms;
using HealthCareApplication.Controller;
using HealthCareApplication.Properties;

namespace HealthCareApplication.View
{
    /// <summary>
    ///     Creates the Test or Diagnosis Form class
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class TestOrDiagForm : Form
    {
        #region Data members

        /// <summary>
        ///     The check rout identifier
        /// </summary>
        public static int CheckRoutId;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="TestOrDiagForm" /> class.
        /// </summary>
        public TestOrDiagForm()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        private void addDiagnosisButton_Click(object sender, EventArgs e)
        {
            try
            {
                var diagController = new DiagnosisController();
                if (diagController.DoesDioAlreadyExistForGivenAppointment(CheckRoutId))
                {
                    MessageBox.Show(
                        Resources
                            .TestOrDiagForm_addDiagnosisButton_Click_A_diagnosis_already_exists_for_this_patient_for_this_appointment_);
                }
                else
                {
                    var form = new DiagnosisForm();
                    form.Show();
                }
            }
            catch (SystemException ecp)
            {
                MessageBox.Show(string.Format(Resources.AdminForm_sqlSubmitButton_Click_An_error_occurred___0_,
                    ecp.Message));
            }
        }

        private void addTestButton_Click(object sender, EventArgs e)
        {
            var form = new TestsForm();
            form.Show();
        }

        #endregion
    }
}