﻿using System;
using System.Windows.Forms;
using HealthCareApplication.Controller;
using HealthCareApplication.Model;
using HealthCareApplication.Properties;

namespace HealthCareApplication.View
{
    /// <summary>
    ///     Creates the loginForm class
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class LoginForm : Form
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="LoginForm" /> class.
        /// </summary>
        public LoginForm()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        private void okButton_Click(object sender, EventArgs e)
        {
            var adminController = new AdminController();
            var nurseController = new NurseController();

            var userName = this.userNameTextBox.Text;
            var password = this.passwordTextBox.Text;

            var rot = new RotEncription(13);
            var encriptedPassword = rot.EncriptText(password);

            this.userNameTextBox.Text = string.Empty;
            this.passwordTextBox.Text = string.Empty;

            if (this.radioButton1.Checked & nurseController.CheckIfNurseIsLoggedInCorrectly(userName, encriptedPassword))
            {
                Hide();

                using (var form2 = new NurseForm())
                {
                    try
                    {
                        var theNurse = nurseController.GetNurseFromNurseIdForLogin(userName);
                        RoutineCheckForm.NurseId = theNurse.NurseId;
                        var loginNameForNurseForm = theNurse.FName + " " + theNurse.LName;
                        form2.SetLoginLabel(loginNameForNurseForm);
                        form2.ShowDialog();
                    }
                    catch (SystemException ecp)
                    {
                        MessageBox.Show(string.Format(Resources.AdminForm_sqlSubmitButton_Click_An_error_occurred___0_,
                            ecp.Message));
                    }
                }

                Show();
            }
            else if (this.radioButton2.Checked & adminController.CheckIfAdminIsLoggedInCorrectly(userName, encriptedPassword))
            {
                Hide();

                using (var form3 = new AdminForm())
                {
                    try
                    {
                        var theAdmin = adminController.GetAdminWithLoginUserNameInformation(userName);
                        var loginNameForAdminForm = theAdmin.FName + " " + theAdmin.LName;
                        form3.SetLoginLabel(loginNameForAdminForm);
                        form3.ShowDialog();
                    }
                    catch (SystemException ecp)
                    {
                        MessageBox.Show(string.Format(Resources.AdminForm_sqlSubmitButton_Click_An_error_occurred___0_,
                            ecp.Message));
                    }
                }

                Show();
            }
            else
            {
                MessageBox.Show(Resources.LoginForm_okButton_Click_Incorrect_credentials_);
            }
        }

        #endregion
    }
}