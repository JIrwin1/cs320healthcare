﻿namespace HealthCareApplication.View
{
    partial class UpdateTestResultsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.resultsRichTextBox = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.submitTestResultsButton = new System.Windows.Forms.Button();
            this.cancelTestResultsButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(288, 126);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(8, 8);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // resultsRichTextBox
            // 
            this.resultsRichTextBox.Location = new System.Drawing.Point(14, 44);
            this.resultsRichTextBox.MaxLength = 45;
            this.resultsRichTextBox.Name = "resultsRichTextBox";
            this.resultsRichTextBox.Size = new System.Drawing.Size(258, 146);
            this.resultsRichTextBox.TabIndex = 1;
            this.resultsRichTextBox.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Please enter Test Results :";
            // 
            // submitTestResultsButton
            // 
            this.submitTestResultsButton.Location = new System.Drawing.Point(197, 196);
            this.submitTestResultsButton.Name = "submitTestResultsButton";
            this.submitTestResultsButton.Size = new System.Drawing.Size(75, 23);
            this.submitTestResultsButton.TabIndex = 3;
            this.submitTestResultsButton.Text = "Submit";
            this.submitTestResultsButton.UseVisualStyleBackColor = true;
            this.submitTestResultsButton.Click += new System.EventHandler(this.submitTestResultsButton_Click);
            // 
            // cancelTestResultsButton
            // 
            this.cancelTestResultsButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelTestResultsButton.Location = new System.Drawing.Point(15, 196);
            this.cancelTestResultsButton.Name = "cancelTestResultsButton";
            this.cancelTestResultsButton.Size = new System.Drawing.Size(75, 23);
            this.cancelTestResultsButton.TabIndex = 4;
            this.cancelTestResultsButton.Text = "Cancel";
            this.cancelTestResultsButton.UseVisualStyleBackColor = true;
            this.cancelTestResultsButton.Click += new System.EventHandler(this.cancelTestResultsButton_Click);
            // 
            // UpdateTestResultsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 236);
            this.Controls.Add(this.cancelTestResultsButton);
            this.Controls.Add(this.submitTestResultsButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.resultsRichTextBox);
            this.Controls.Add(this.richTextBox1);
            this.Name = "UpdateTestResultsForm";
            this.Text = "UpdateTestResultsForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.RichTextBox resultsRichTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button submitTestResultsButton;
        private System.Windows.Forms.Button cancelTestResultsButton;
    }
}