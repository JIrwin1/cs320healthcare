﻿using System;
using System.Windows.Forms;
using HealthCareApplication.Controller;
using HealthCareApplication.Properties;

namespace HealthCareApplication.View
{
    /// <summary>
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class UpdateDiagnosisForm : Form
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the update diagnosis identifier.
        /// </summary>
        /// <value>
        ///     The update diagnosis identifier.
        /// </value>
        public static int UpdateDiagnosisId { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="UpdateDiagnosisForm" /> class.
        /// </summary>
        public UpdateDiagnosisForm()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        private void updateDiagnosisSubmitButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.updateDiagnosisTextBox.Text == string.Empty)
                {
                    MessageBox.Show(
                        Resources
                            .UpdateDiagnosisForm_updateDiagnosisSubmitButton_Click_You_must_enter_something_to_submit_a_diagnosis_update_);
                }
                else
                {
                    var diagController = new DiagnosisController();
                    diagController.UpdateDioResults(UpdateDiagnosisId, this.updateDiagnosisTextBox.Text);
                    MessageBox.Show(
                        Resources.UpdateDiagnosisForm_updateDiagnosisSubmitButton_Click_Diagnosis_was_updated_);
                    Close();
                }
            }
            catch (SystemException ecp)
            {
                MessageBox.Show(string.Format(Resources.AdminForm_sqlSubmitButton_Click_An_error_occurred___0_,
                    ecp.Message));
            }
        }

        private void updateDiagnosisCancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion
    }
}