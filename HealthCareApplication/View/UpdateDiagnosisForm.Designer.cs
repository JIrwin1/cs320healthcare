﻿namespace HealthCareApplication.View
{
    partial class UpdateDiagnosisForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.updateDiagnosisLabel = new System.Windows.Forms.Label();
            this.updateDiagnosisTextBox = new System.Windows.Forms.RichTextBox();
            this.updateDiagnosisSubmitButton = new System.Windows.Forms.Button();
            this.updateDiagnosisCancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // updateDiagnosisLabel
            // 
            this.updateDiagnosisLabel.AutoSize = true;
            this.updateDiagnosisLabel.Location = new System.Drawing.Point(26, 26);
            this.updateDiagnosisLabel.Name = "updateDiagnosisLabel";
            this.updateDiagnosisLabel.Size = new System.Drawing.Size(122, 13);
            this.updateDiagnosisLabel.TabIndex = 0;
            this.updateDiagnosisLabel.Text = "Enter the final diagnosis:";
            // 
            // updateDiagnosisTextBox
            // 
            this.updateDiagnosisTextBox.Location = new System.Drawing.Point(29, 61);
            this.updateDiagnosisTextBox.MaxLength = 60;
            this.updateDiagnosisTextBox.Name = "updateDiagnosisTextBox";
            this.updateDiagnosisTextBox.Size = new System.Drawing.Size(233, 142);
            this.updateDiagnosisTextBox.TabIndex = 1;
            this.updateDiagnosisTextBox.Text = "";
            // 
            // updateDiagnosisSubmitButton
            // 
            this.updateDiagnosisSubmitButton.Location = new System.Drawing.Point(187, 209);
            this.updateDiagnosisSubmitButton.Name = "updateDiagnosisSubmitButton";
            this.updateDiagnosisSubmitButton.Size = new System.Drawing.Size(75, 23);
            this.updateDiagnosisSubmitButton.TabIndex = 2;
            this.updateDiagnosisSubmitButton.Text = "Submit";
            this.updateDiagnosisSubmitButton.UseVisualStyleBackColor = true;
            this.updateDiagnosisSubmitButton.Click += new System.EventHandler(this.updateDiagnosisSubmitButton_Click);
            // 
            // updateDiagnosisCancelButton
            // 
            this.updateDiagnosisCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.updateDiagnosisCancelButton.Location = new System.Drawing.Point(29, 209);
            this.updateDiagnosisCancelButton.Name = "updateDiagnosisCancelButton";
            this.updateDiagnosisCancelButton.Size = new System.Drawing.Size(75, 23);
            this.updateDiagnosisCancelButton.TabIndex = 3;
            this.updateDiagnosisCancelButton.Text = "Cancel";
            this.updateDiagnosisCancelButton.UseVisualStyleBackColor = true;
            this.updateDiagnosisCancelButton.Click += new System.EventHandler(this.updateDiagnosisCancelButton_Click);
            // 
            // UpdateDiagnosisForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.updateDiagnosisCancelButton);
            this.Controls.Add(this.updateDiagnosisSubmitButton);
            this.Controls.Add(this.updateDiagnosisTextBox);
            this.Controls.Add(this.updateDiagnosisLabel);
            this.Name = "UpdateDiagnosisForm";
            this.Text = "UpdateDiagnosisForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label updateDiagnosisLabel;
        private System.Windows.Forms.RichTextBox updateDiagnosisTextBox;
        private System.Windows.Forms.Button updateDiagnosisSubmitButton;
        private System.Windows.Forms.Button updateDiagnosisCancelButton;
    }
}