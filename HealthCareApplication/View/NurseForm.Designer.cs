﻿namespace HealthCareApplication.View
{
    partial class NurseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.logOutButtonNurse = new System.Windows.Forms.Button();
            this.registerPatientButton = new System.Windows.Forms.Button();
            this.searchPatients = new System.Windows.Forms.Button();
            this.searchPatientsGroupBox = new System.Windows.Forms.GroupBox();
            this.dobCheckBox = new System.Windows.Forms.CheckBox();
            this.lastNameCheckBox = new System.Windows.Forms.CheckBox();
            this.fNameCheckBox = new System.Windows.Forms.CheckBox();
            this.dateOfBirthSearchPicker1 = new System.Windows.Forms.DateTimePicker();
            this.lastNameSearchBox = new System.Windows.Forms.TextBox();
            this.firstNameSearchBox = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.clearNurseButton = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.searchPatientsGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // logOutButtonNurse
            // 
            this.logOutButtonNurse.Location = new System.Drawing.Point(341, 413);
            this.logOutButtonNurse.Name = "logOutButtonNurse";
            this.logOutButtonNurse.Size = new System.Drawing.Size(75, 23);
            this.logOutButtonNurse.TabIndex = 1;
            this.logOutButtonNurse.Text = "Log Out";
            this.logOutButtonNurse.UseVisualStyleBackColor = true;
            this.logOutButtonNurse.Click += new System.EventHandler(this.closeButtonNurse_Click);
            // 
            // registerPatientButton
            // 
            this.registerPatientButton.Location = new System.Drawing.Point(34, 47);
            this.registerPatientButton.Name = "registerPatientButton";
            this.registerPatientButton.Size = new System.Drawing.Size(108, 23);
            this.registerPatientButton.TabIndex = 2;
            this.registerPatientButton.Text = "Register Patient";
            this.registerPatientButton.UseVisualStyleBackColor = true;
            this.registerPatientButton.Click += new System.EventHandler(this.registerPatientButton_Click);
            // 
            // searchPatients
            // 
            this.searchPatients.Location = new System.Drawing.Point(319, 181);
            this.searchPatients.Name = "searchPatients";
            this.searchPatients.Size = new System.Drawing.Size(110, 23);
            this.searchPatients.TabIndex = 4;
            this.searchPatients.Text = "Search Patients";
            this.searchPatients.UseVisualStyleBackColor = true;
            this.searchPatients.Click += new System.EventHandler(this.searchPatients_Click);
            // 
            // searchPatientsGroupBox
            // 
            this.searchPatientsGroupBox.Controls.Add(this.dobCheckBox);
            this.searchPatientsGroupBox.Controls.Add(this.lastNameCheckBox);
            this.searchPatientsGroupBox.Controls.Add(this.fNameCheckBox);
            this.searchPatientsGroupBox.Controls.Add(this.dateOfBirthSearchPicker1);
            this.searchPatientsGroupBox.Controls.Add(this.lastNameSearchBox);
            this.searchPatientsGroupBox.Controls.Add(this.firstNameSearchBox);
            this.searchPatientsGroupBox.Location = new System.Drawing.Point(171, 47);
            this.searchPatientsGroupBox.Name = "searchPatientsGroupBox";
            this.searchPatientsGroupBox.Size = new System.Drawing.Size(258, 101);
            this.searchPatientsGroupBox.TabIndex = 5;
            this.searchPatientsGroupBox.TabStop = false;
            this.searchPatientsGroupBox.Text = "Search For Patients By:";
            // 
            // dobCheckBox
            // 
            this.dobCheckBox.AutoSize = true;
            this.dobCheckBox.Location = new System.Drawing.Point(33, 73);
            this.dobCheckBox.Name = "dobCheckBox";
            this.dobCheckBox.Size = new System.Drawing.Size(88, 17);
            this.dobCheckBox.TabIndex = 12;
            this.dobCheckBox.Text = "Date of Birth:";
            this.dobCheckBox.UseVisualStyleBackColor = true;
            // 
            // lastNameCheckBox
            // 
            this.lastNameCheckBox.AutoSize = true;
            this.lastNameCheckBox.Location = new System.Drawing.Point(33, 47);
            this.lastNameCheckBox.Name = "lastNameCheckBox";
            this.lastNameCheckBox.Size = new System.Drawing.Size(80, 17);
            this.lastNameCheckBox.TabIndex = 13;
            this.lastNameCheckBox.Text = "Last Name:";
            this.lastNameCheckBox.UseVisualStyleBackColor = true;
            // 
            // fNameCheckBox
            // 
            this.fNameCheckBox.AutoSize = true;
            this.fNameCheckBox.Location = new System.Drawing.Point(33, 21);
            this.fNameCheckBox.Name = "fNameCheckBox";
            this.fNameCheckBox.Size = new System.Drawing.Size(79, 17);
            this.fNameCheckBox.TabIndex = 11;
            this.fNameCheckBox.Text = "First Name:";
            this.fNameCheckBox.UseVisualStyleBackColor = true;
            // 
            // dateOfBirthSearchPicker1
            // 
            this.dateOfBirthSearchPicker1.CustomFormat = "MM-dd-yyyy";
            this.dateOfBirthSearchPicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateOfBirthSearchPicker1.Location = new System.Drawing.Point(158, 70);
            this.dateOfBirthSearchPicker1.Name = "dateOfBirthSearchPicker1";
            this.dateOfBirthSearchPicker1.Size = new System.Drawing.Size(100, 20);
            this.dateOfBirthSearchPicker1.TabIndex = 10;
            // 
            // lastNameSearchBox
            // 
            this.lastNameSearchBox.Location = new System.Drawing.Point(158, 44);
            this.lastNameSearchBox.Name = "lastNameSearchBox";
            this.lastNameSearchBox.Size = new System.Drawing.Size(100, 20);
            this.lastNameSearchBox.TabIndex = 1;
            // 
            // firstNameSearchBox
            // 
            this.firstNameSearchBox.Location = new System.Drawing.Point(158, 18);
            this.firstNameSearchBox.Name = "firstNameSearchBox";
            this.firstNameSearchBox.Size = new System.Drawing.Size(100, 20);
            this.firstNameSearchBox.TabIndex = 0;
            // 
            // comboBox1
            // 
            this.comboBox1.AllowDrop = true;
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Search for Patient ID:",
            "Search for Appointments:",
            "Search for Routine Checks",
            "Search for Past Diagnosis:",
            "Search for Tests:"});
            this.comboBox1.Location = new System.Drawing.Point(184, 154);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(245, 21);
            this.comboBox1.TabIndex = 9;
            // 
            // clearNurseButton
            // 
            this.clearNurseButton.Location = new System.Drawing.Point(34, 413);
            this.clearNurseButton.Name = "clearNurseButton";
            this.clearNurseButton.Size = new System.Drawing.Size(75, 23);
            this.clearNurseButton.TabIndex = 11;
            this.clearNurseButton.Text = "Clear Results";
            this.clearNurseButton.UseVisualStyleBackColor = true;
            this.clearNurseButton.Click += new System.EventHandler(this.clearNurseButton_Click_1);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowDrop = true;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(34, 219);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
            this.dataGridView1.Size = new System.Drawing.Size(395, 188);
            this.dataGridView1.TabIndex = 12;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // NurseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 445);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.clearNurseButton);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.searchPatientsGroupBox);
            this.Controls.Add(this.searchPatients);
            this.Controls.Add(this.registerPatientButton);
            this.Controls.Add(this.logOutButtonNurse);
            this.Name = "NurseForm";
            this.Text = "NurseForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.NurseForm_FormClosed);
            this.searchPatientsGroupBox.ResumeLayout(false);
            this.searchPatientsGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button logOutButtonNurse;
        private System.Windows.Forms.Button registerPatientButton;
        private System.Windows.Forms.Button searchPatients;
        private System.Windows.Forms.GroupBox searchPatientsGroupBox;
        private System.Windows.Forms.TextBox lastNameSearchBox;
        private System.Windows.Forms.TextBox firstNameSearchBox;
        private System.Windows.Forms.DateTimePicker dateOfBirthSearchPicker1;
        private System.Windows.Forms.CheckBox dobCheckBox;
        private System.Windows.Forms.CheckBox lastNameCheckBox;
        private System.Windows.Forms.CheckBox fNameCheckBox;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button clearNurseButton;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}