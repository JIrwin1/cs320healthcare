﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using HealthCareApplication.Controller;
using HealthCareApplication.DAL.Repository;
using HealthCareApplication.Model;
using HealthCareApplication.Properties;

namespace HealthCareApplication.View
{
    /// <summary>
    ///     Creates the nurseform class
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class NurseForm : Form
    {
        #region Data members

        private readonly PatientReposiotry patRep = new PatientReposiotry();

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the name of the nurse log in.
        /// </summary>
        /// <value>
        ///     The name of the nurse log in.
        /// </value>
        public string NurseLogInName { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="NurseForm" /> class.
        /// </summary>
        public NurseForm()
        {
            this.InitializeComponent();
            this.comboBox1.SelectedIndex = 0;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Sets the login label.
        /// </summary>
        /// <param name="labelName">Name of the label.</param>
        public void SetLoginLabel(string labelName)
        {
            Text = labelName;
            Update();
        }

        private void closeButtonNurse_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void NurseForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void registerPatientButton_Click(object sender, EventArgs e)
        {
            var form = new RegisterPatientForm();

            form.Show();
        }

        private void searchPatients_Click(object sender, EventArgs e)
        {
            try
            {
                var firstName = this.firstNameSearchBox.Text;
                var lastName = this.lastNameSearchBox.Text;
                var dob = this.dateOfBirthSearchPicker1.Value.Date;
                var patientController = new PatientController();
                var appointmentController = new AppointmentController();
                var diagnosisController = new DiagnosisController();
                var routineChecksController = new RoutineChecksController();
                var labresultsController = new LabResultController();

                if (this.fNameCheckBox.Checked && !this.lastNameCheckBox.Checked && !this.dobCheckBox.Checked)
                {
                    var patients = patientController.GetPatientInformationFromSearchByFirstName(firstName);
                    this.getInformation(patients, appointmentController, diagnosisController, routineChecksController,
                        labresultsController);
                }

                else if (this.fNameCheckBox.Checked && this.lastNameCheckBox.Checked && !this.dobCheckBox.Checked)
                {
                    var patients = patientController.GetPatientInformationFromSearchByFirstLastName(firstName, lastName);
                    this.getInformation(patients, appointmentController, diagnosisController, routineChecksController,
                        labresultsController);
                }

                else if (this.fNameCheckBox.Checked && !this.lastNameCheckBox.Checked && this.dobCheckBox.Checked)
                {
                    var patients = patientController.GetPatientInformationFromSearchByFirstNameDob(firstName, dob.Date);
                    this.getInformation(patients, appointmentController, diagnosisController, routineChecksController,
                        labresultsController);
                }

                else if (!this.fNameCheckBox.Checked && !this.lastNameCheckBox.Checked && this.dobCheckBox.Checked)
                {
                    var patients = patientController.GetPatientInformationFromSearchByDob(dob.Date);
                    this.getInformation(patients, appointmentController, diagnosisController, routineChecksController,
                        labresultsController);
                }

                else if (!this.fNameCheckBox.Checked && this.lastNameCheckBox.Checked && this.dobCheckBox.Checked)
                {
                    var patients = patientController.GetPatientInformationFromSearchByLastNameDob(lastName, dob.Date);
                    this.getInformation(patients, appointmentController, diagnosisController, routineChecksController,
                        labresultsController);
                }

                else if (this.fNameCheckBox.Checked && this.lastNameCheckBox.Checked && this.dobCheckBox.Checked)
                {
                    var patients = patientController.GetPatientInformationFromSearchByFirstLastNameDob(firstName,
                        lastName,
                        dob.Date);
                    this.getInformation(patients, appointmentController, diagnosisController, routineChecksController,
                        labresultsController);
                }

                else if (!this.fNameCheckBox.Checked && this.lastNameCheckBox.Checked && !this.dobCheckBox.Checked)
                {
                    var patients = patientController.GetPatientInformationFromSearchByLastName(lastName);
                    this.getInformation(patients, appointmentController, diagnosisController, routineChecksController,
                        labresultsController);
                }
            }
            catch (SystemException ecp)
            {
                MessageBox.Show(string.Format(Resources.AdminForm_sqlSubmitButton_Click_An_error_occurred___0_,
                    ecp.Message));
            }
        }

        private void getInformation(List<Patient> patients, AppointmentController appointmentController,
            DiagnosisController diagnosisController, RoutineChecksController routineChecksController,
            LabResultController labresultsController)
        {
            try
            {
                switch (this.comboBox1.SelectedIndex)
                {
                    case 0:
                        this.dataGridView1.DataSource = patients;
                        break;

                    case 1:
                        var appointments = new List<Appointment>();

                        foreach (var patient in patients)
                        {
                            var theAppointments = appointmentController.GetAppointsOfPatient(patient);

                            foreach (var aAppointment in theAppointments)
                            {
                                appointments.Add(aAppointment);
                            }
                        }

                        this.dataGridView1.DataSource = appointments;
                        break;

                    case 2:
                        var routineChecks = new List<RoutineChecks>();

                        foreach (var patient in patients)
                        {
                            var theChecks = routineChecksController.GetRoutineChecksForPatient(patient);

                            foreach (var acheck in theChecks)
                            {
                                routineChecks.Add(acheck);
                            }
                        }
                        this.dataGridView1.DataSource = routineChecks;
                        break;

                    case 3:
                        var diagnosises = new List<Diagnosis>();

                        foreach (var patient in patients)
                        {
                            var theDiagnosises = diagnosisController.GetDiagnoses(patient);

                            foreach (var aDiagnosises in theDiagnosises)
                            {
                                diagnosises.Add(aDiagnosises);
                            }
                        }

                        this.dataGridView1.DataSource = diagnosises;
                        break;

                    case 4:
                        var tests = new List<LabResults>();

                        foreach (var patient in patients)
                        {
                            var theTests = labresultsController.GetTestForPatient(patient);

                            foreach (var test in theTests)
                            {
                                tests.Add(test);
                            }
                        }

                        this.dataGridView1.DataSource = tests;
                        break;
                }
            }
            catch (SystemException ecp)
            {
                MessageBox.Show(string.Format(Resources.AdminForm_sqlSubmitButton_Click_An_error_occurred___0_,
                    ecp.Message));
            }
        }

        private void clearNurseButton_Click_1(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = null;
            this.dataGridView1.Rows.Clear();
            Update();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (this.comboBox1.SelectedIndex == 0)
                {
                    var row = this.dataGridView1.Rows[e.RowIndex];
                    var patientId = row.Cells[0].Value.ToString();

                    var form = new ScheduleAppointment {PatientId = Convert.ToInt32(patientId)};
                    form.ShowDialog();
                }
                if (this.comboBox1.SelectedIndex == 1)
                {
                    var routineChecksController = new RoutineChecksController();
                    var row = this.dataGridView1.Rows[e.RowIndex];
                    var appointmentId = row.Cells[0].Value.ToString();
                    RoutineCheckForm.RCheckAppointmentId = Convert.ToInt32(appointmentId);
                    if (routineChecksController.DoesRoutineCheckExist(Convert.ToInt32(appointmentId)))
                    {
                        MessageBox.Show(
                            Resources.RoutineCheckForm_submitRoutineChecksButton_Click_Already_has_a_check_in_database_);
                    }
                    else
                    {
                        var form = new RoutineCheckForm();
                        form.ShowDialog();
                    }
                }

                if (this.comboBox1.SelectedIndex == 2)
                {
                    var row = this.dataGridView1.Rows[e.RowIndex];
                    var routineCheckId = row.Cells[0].Value.ToString();
                    TestsForm.TestRoutineCheckId = Convert.ToInt32(routineCheckId);
                    TestOrDiagForm.CheckRoutId = Convert.ToInt32(routineCheckId);
                    DiagnosisForm.DiagnosisRoutineCheckId = Convert.ToInt32(routineCheckId);
                    var form = new TestOrDiagForm();
                    form.ShowDialog();
                }
                if (this.comboBox1.SelectedIndex == 3)
                {
                    DiagnosisController diagnosisController = new DiagnosisController();
                    var row = this.dataGridView1.Rows[e.RowIndex];
                    var diagnosisId = row.Cells[0].Value.ToString();
                    UpdateDiagnosisForm.UpdateDiagnosisId = Convert.ToInt32(diagnosisId);

                    if (diagnosisController.CheckToSeeIfDioDoesntHaveAFinalDio(Convert.ToInt32(diagnosisId)))
                    {
                        var form = new UpdateDiagnosisForm();
                        form.ShowDialog();
                       

                    }
                    else
                    {
                        MessageBox.Show(
                            Resources
                                .NurseForm_dataGridView1_CellContentClick_The_Diagnosis_has_already_recieved_it_s_final_update_);
                    }
                }
                if (this.comboBox1.SelectedIndex == 4)
                {
                    LabResultController results = new LabResultController();
                    var row = this.dataGridView1.Rows[e.RowIndex];
                    var testId = row.Cells[0].Value.ToString();
                    UpdateTestResultsForm.UpdateTestResultsId = Convert.ToInt32(testId);

                    UpdateTestResultsForm.UpdateTestResultsId = Convert.ToInt32(testId);
                    if (results.DoesLabHaveNoResult(Convert.ToInt32(testId)))
                    {

                        var form = new UpdateTestResultsForm();
                        form.ShowDialog();
                    }
                    else
                    {
                       
                        MessageBox.Show(Resources.NurseForm_dataGridView1_CellContentClick_This_test_already_has_results_);

                    }
                }
            }
            catch (SystemException ecp)
            {
                MessageBox.Show(string.Format(Resources.AdminForm_sqlSubmitButton_Click_An_error_occurred___0_,
                    ecp.Message));
            }
        }



        #endregion
    }
}