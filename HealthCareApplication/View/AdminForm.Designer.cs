﻿namespace HealthCareApplication.View
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.closeButtonAdmin = new System.Windows.Forms.Button();
            this.adminSqlTextBox = new System.Windows.Forms.RichTextBox();
            this.adminSqlResultsDataGridView = new System.Windows.Forms.DataGridView();
            this.sqlSubmitButton = new System.Windows.Forms.Button();
            this.sqlClearButton = new System.Windows.Forms.Button();
            this.sqlResultsClearButton = new System.Windows.Forms.Button();
            this.adminDateSearchBox = new System.Windows.Forms.GroupBox();
            this.searchBetweenDatesButton = new System.Windows.Forms.Button();
            this.endDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.beginSearchDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.endDateSearchLabel = new System.Windows.Forms.Label();
            this.beginDateSearch = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.adminSqlResultsDataGridView)).BeginInit();
            this.adminDateSearchBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // closeButtonAdmin
            // 
            this.closeButtonAdmin.Location = new System.Drawing.Point(547, 569);
            this.closeButtonAdmin.Name = "closeButtonAdmin";
            this.closeButtonAdmin.Size = new System.Drawing.Size(75, 23);
            this.closeButtonAdmin.TabIndex = 1;
            this.closeButtonAdmin.Text = "Log Out";
            this.closeButtonAdmin.UseVisualStyleBackColor = true;
            this.closeButtonAdmin.Click += new System.EventHandler(this.closeButtonAdmin_Click);
            // 
            // adminSqlTextBox
            // 
            this.adminSqlTextBox.Location = new System.Drawing.Point(15, 25);
            this.adminSqlTextBox.Name = "adminSqlTextBox";
            this.adminSqlTextBox.Size = new System.Drawing.Size(391, 237);
            this.adminSqlTextBox.TabIndex = 2;
            this.adminSqlTextBox.Text = "";
            // 
            // adminSqlResultsDataGridView
            // 
            this.adminSqlResultsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.adminSqlResultsDataGridView.Location = new System.Drawing.Point(15, 313);
            this.adminSqlResultsDataGridView.Name = "adminSqlResultsDataGridView";
            this.adminSqlResultsDataGridView.ReadOnly = true;
            this.adminSqlResultsDataGridView.Size = new System.Drawing.Size(618, 229);
            this.adminSqlResultsDataGridView.TabIndex = 3;
            // 
            // sqlSubmitButton
            // 
            this.sqlSubmitButton.Location = new System.Drawing.Point(302, 268);
            this.sqlSubmitButton.Name = "sqlSubmitButton";
            this.sqlSubmitButton.Size = new System.Drawing.Size(75, 23);
            this.sqlSubmitButton.TabIndex = 4;
            this.sqlSubmitButton.Text = "Submit";
            this.sqlSubmitButton.UseVisualStyleBackColor = true;
            this.sqlSubmitButton.Click += new System.EventHandler(this.sqlSubmitButton_Click);
            // 
            // sqlClearButton
            // 
            this.sqlClearButton.Location = new System.Drawing.Point(15, 268);
            this.sqlClearButton.Name = "sqlClearButton";
            this.sqlClearButton.Size = new System.Drawing.Size(113, 23);
            this.sqlClearButton.TabIndex = 5;
            this.sqlClearButton.Text = "Clear SQL";
            this.sqlClearButton.UseVisualStyleBackColor = true;
            this.sqlClearButton.Click += new System.EventHandler(this.sqlClearButton_Click);
            // 
            // sqlResultsClearButton
            // 
            this.sqlResultsClearButton.Location = new System.Drawing.Point(15, 569);
            this.sqlResultsClearButton.Name = "sqlResultsClearButton";
            this.sqlResultsClearButton.Size = new System.Drawing.Size(113, 23);
            this.sqlResultsClearButton.TabIndex = 6;
            this.sqlResultsClearButton.Text = "Clear Result";
            this.sqlResultsClearButton.UseVisualStyleBackColor = true;
            this.sqlResultsClearButton.Click += new System.EventHandler(this.sqlResultsClearButton_Click);
            // 
            // adminDateSearchBox
            // 
            this.adminDateSearchBox.Controls.Add(this.searchBetweenDatesButton);
            this.adminDateSearchBox.Controls.Add(this.endDateTimePicker);
            this.adminDateSearchBox.Controls.Add(this.beginSearchDateTimePicker);
            this.adminDateSearchBox.Controls.Add(this.endDateSearchLabel);
            this.adminDateSearchBox.Controls.Add(this.beginDateSearch);
            this.adminDateSearchBox.Location = new System.Drawing.Point(412, 41);
            this.adminDateSearchBox.Name = "adminDateSearchBox";
            this.adminDateSearchBox.Size = new System.Drawing.Size(221, 201);
            this.adminDateSearchBox.TabIndex = 7;
            this.adminDateSearchBox.TabStop = false;
            this.adminDateSearchBox.Text = "Search Between:";
            // 
            // searchBetweenDatesButton
            // 
            this.searchBetweenDatesButton.Location = new System.Drawing.Point(140, 172);
            this.searchBetweenDatesButton.Name = "searchBetweenDatesButton";
            this.searchBetweenDatesButton.Size = new System.Drawing.Size(75, 23);
            this.searchBetweenDatesButton.TabIndex = 4;
            this.searchBetweenDatesButton.Text = "Search";
            this.searchBetweenDatesButton.UseVisualStyleBackColor = true;
            this.searchBetweenDatesButton.Click += new System.EventHandler(this.searchBetweenDatesButton_Click);
            // 
            // endDateTimePicker
            // 
            this.endDateTimePicker.CustomFormat = "MM-dd-yyyy";
            this.endDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.endDateTimePicker.Location = new System.Drawing.Point(118, 100);
            this.endDateTimePicker.Name = "endDateTimePicker";
            this.endDateTimePicker.Size = new System.Drawing.Size(97, 20);
            this.endDateTimePicker.TabIndex = 3;
            // 
            // beginSearchDateTimePicker
            // 
            this.beginSearchDateTimePicker.CustomFormat = "MM-dd-yyyy";
            this.beginSearchDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.beginSearchDateTimePicker.Location = new System.Drawing.Point(118, 33);
            this.beginSearchDateTimePicker.Name = "beginSearchDateTimePicker";
            this.beginSearchDateTimePicker.Size = new System.Drawing.Size(97, 20);
            this.beginSearchDateTimePicker.TabIndex = 2;
            // 
            // endDateSearchLabel
            // 
            this.endDateSearchLabel.AutoSize = true;
            this.endDateSearchLabel.Location = new System.Drawing.Point(38, 106);
            this.endDateSearchLabel.Name = "endDateSearchLabel";
            this.endDateSearchLabel.Size = new System.Drawing.Size(42, 13);
            this.endDateSearchLabel.TabIndex = 1;
            this.endDateSearchLabel.Text = "End At:";
            // 
            // beginDateSearch
            // 
            this.beginDateSearch.AutoSize = true;
            this.beginDateSearch.Location = new System.Drawing.Point(30, 40);
            this.beginDateSearch.Name = "beginDateSearch";
            this.beginDateSearch.Size = new System.Drawing.Size(50, 13);
            this.beginDateSearch.TabIndex = 0;
            this.beginDateSearch.Text = "Begin At:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter SQL quesries here:";
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 604);
            this.Controls.Add(this.adminDateSearchBox);
            this.Controls.Add(this.sqlResultsClearButton);
            this.Controls.Add(this.sqlClearButton);
            this.Controls.Add(this.sqlSubmitButton);
            this.Controls.Add(this.adminSqlResultsDataGridView);
            this.Controls.Add(this.adminSqlTextBox);
            this.Controls.Add(this.closeButtonAdmin);
            this.Controls.Add(this.label1);
            this.Name = "AdminForm";
            this.Text = "AdminForm";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AdminForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.adminSqlResultsDataGridView)).EndInit();
            this.adminDateSearchBox.ResumeLayout(false);
            this.adminDateSearchBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button closeButtonAdmin;
        private System.Windows.Forms.RichTextBox adminSqlTextBox;
        private System.Windows.Forms.DataGridView adminSqlResultsDataGridView;
        private System.Windows.Forms.Button sqlSubmitButton;
        private System.Windows.Forms.Button sqlClearButton;
        private System.Windows.Forms.Button sqlResultsClearButton;
        private System.Windows.Forms.GroupBox adminDateSearchBox;
        private System.Windows.Forms.DateTimePicker beginSearchDateTimePicker;
        private System.Windows.Forms.Label endDateSearchLabel;
        private System.Windows.Forms.Label beginDateSearch;
        private System.Windows.Forms.DateTimePicker endDateTimePicker;
        private System.Windows.Forms.Button searchBetweenDatesButton;
        private System.Windows.Forms.Label label1;
    }
}