﻿namespace HealthCareApplication.View
{
    partial class TestOrDiagForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.addTestButton = new System.Windows.Forms.Button();
            this.addDiagnosisButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // addTestButton
            // 
            this.addTestButton.Location = new System.Drawing.Point(47, 32);
            this.addTestButton.Name = "addTestButton";
            this.addTestButton.Size = new System.Drawing.Size(115, 67);
            this.addTestButton.TabIndex = 0;
            this.addTestButton.Text = "Add Test";
            this.addTestButton.UseVisualStyleBackColor = true;
            this.addTestButton.Click += new System.EventHandler(this.addTestButton_Click);
            // 
            // addDiagnosisButton
            // 
            this.addDiagnosisButton.Location = new System.Drawing.Point(47, 127);
            this.addDiagnosisButton.Name = "addDiagnosisButton";
            this.addDiagnosisButton.Size = new System.Drawing.Size(115, 67);
            this.addDiagnosisButton.TabIndex = 1;
            this.addDiagnosisButton.Text = "Add Diagnosis";
            this.addDiagnosisButton.UseVisualStyleBackColor = true;
            this.addDiagnosisButton.Click += new System.EventHandler(this.addDiagnosisButton_Click);
            // 
            // TestOrDiagForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(221, 226);
            this.Controls.Add(this.addDiagnosisButton);
            this.Controls.Add(this.addTestButton);
            this.Name = "TestOrDiagForm";
            this.Text = "TestOrDiagForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button addTestButton;
        private System.Windows.Forms.Button addDiagnosisButton;
    }
}