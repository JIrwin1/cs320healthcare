﻿using System;
using System.Data;
using System.Windows.Forms;
using HealthCareApplication.Controller;
using HealthCareApplication.Properties;

namespace HealthCareApplication.View
{
    /// <summary>
    ///     Creates the AdminForm
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class AdminForm : Form
    {
        #region Data members

        /// <summary>
        ///     The data
        /// </summary>
        public static DataTable Data;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="AdminForm" /> class.
        /// </summary>
        public AdminForm()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        private void sqlClearButton_Click(object sender, EventArgs e)
        {
            this.adminSqlTextBox.Text = string.Empty;
        }

        private void sqlSubmitButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.adminSqlTextBox.Text == string.Empty)
                {
                    MessageBox.Show(
                        Resources.AdminForm_sqlSubmitButton_Click_You_must_enter_a_valid_SQL_command_to_submit_);
                }
                else
                {
                    var controller = new AdminController();
                    controller.AdminPassInSqlCommand(this.adminSqlTextBox.Text);
                    this.adminSqlResultsDataGridView.DataSource = Data;
                }
            }
            catch (SystemException ecp)
            {
                MessageBox.Show(string.Format(Resources.AdminForm_sqlSubmitButton_Click_An_error_occurred___0_,
                    ecp.Message));
            }
        }

        private void searchBetweenDatesButton_Click(object sender, EventArgs e)
        {
            try
            {
                var appController = new AppointmentController();

                this.adminSqlResultsDataGridView.DataSource =
                    appController.GetVistInformationBetweenThedates(this.beginSearchDateTimePicker.Value,
                        this.endDateTimePicker.Value);
            }
            catch (SystemException ecp)
            {
                MessageBox.Show(string.Format(Resources.AdminForm_sqlSubmitButton_Click_An_error_occurred___0_,
                    ecp.Message));
            }
        }

        private void sqlResultsClearButton_Click(object sender, EventArgs e)
        {
            this.adminSqlResultsDataGridView.DataSource = null;
            this.adminSqlResultsDataGridView.Rows.Clear();
            Update();
        }

        private void closeButtonAdmin_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void AdminForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Application.Exit();
        }

        /// <summary>
        ///     Sets the login label.
        /// </summary>
        /// <param name="labelName">Name of the label.</param>
        public void SetLoginLabel(string labelName)
        {
            Text = labelName;
            Update();
        }

        #endregion
    }
}