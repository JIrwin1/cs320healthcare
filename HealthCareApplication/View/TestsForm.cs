﻿using System;
using System.Linq;
using System.Windows.Forms;
using HealthCareApplication.Controller;
using HealthCareApplication.Properties;

namespace HealthCareApplication.View
{
    /// <summary>
    ///     Creates the Test form class
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class TestsForm : Form
    {
        #region Data members

        /// <summary>
        ///     The test routine check identifier
        /// </summary>
        public static int TestRoutineCheckId;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="TestsForm" /> class.
        /// </summary>
        public TestsForm()
        {
            this.InitializeComponent();
            this.addTestNamesToDropDownMenu();
        }

        #endregion

        #region Methods

        private void testCancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void testAddTestbutton_Click(object sender, EventArgs e)
        {
            try
            {

                {
                    var labController = new LabResultController();
                    var tests = labController.GetTestTypes();

                   
                    var testNumber = tests.FirstOrDefault(x => x.Value == this.testNameComboBox.Text).Key;
                    var controller = new LabResultController();
                    controller.AddResultToTable(TestRoutineCheckId, Convert.ToInt32(testNumber), "",
                        this.scheduleTestDateTimePicker.Value);
                    MessageBox.Show(Resources.TestsForm_testAddTestbutton_Click_Test_successfully_added_);
                    Close();
                }
            }
            catch (SystemException ecp)
            {
                MessageBox.Show(string.Format(Resources.AdminForm_sqlSubmitButton_Click_An_error_occurred___0_,
                    ecp.Message));
            }
        }

        private void addTestNamesToDropDownMenu()
        {
            var labController = new LabResultController();
            var tests = labController.GetTestTypes();
            this.testNameComboBox.DataSource = new BindingSource(tests, null);
            this.testNameComboBox.DisplayMember = "Value";
            //this.testNameComboBox.ValueMember = "Key";
        }

        #endregion
    }
}