﻿namespace HealthCareApplication.View
{
    partial class DiagnosisForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.diagnosisLabel = new System.Windows.Forms.Label();
            this.diagnosisRichTextBox = new System.Windows.Forms.RichTextBox();
            this.submitDiagnosisButton = new System.Windows.Forms.Button();
            this.cancelDiagnosisButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // diagnosisLabel
            // 
            this.diagnosisLabel.AutoSize = true;
            this.diagnosisLabel.Location = new System.Drawing.Point(12, 26);
            this.diagnosisLabel.Name = "diagnosisLabel";
            this.diagnosisLabel.Size = new System.Drawing.Size(53, 13);
            this.diagnosisLabel.TabIndex = 0;
            this.diagnosisLabel.Text = "Diagnosis";
            // 
            // diagnosisRichTextBox
            // 
            this.diagnosisRichTextBox.Location = new System.Drawing.Point(85, 12);
            this.diagnosisRichTextBox.MaxLength = 60;
            this.diagnosisRichTextBox.Name = "diagnosisRichTextBox";
            this.diagnosisRichTextBox.Size = new System.Drawing.Size(199, 114);
            this.diagnosisRichTextBox.TabIndex = 4;
            this.diagnosisRichTextBox.Text = "";
            // 
            // submitDiagnosisButton
            // 
            this.submitDiagnosisButton.Location = new System.Drawing.Point(209, 155);
            this.submitDiagnosisButton.Name = "submitDiagnosisButton";
            this.submitDiagnosisButton.Size = new System.Drawing.Size(75, 23);
            this.submitDiagnosisButton.TabIndex = 5;
            this.submitDiagnosisButton.Text = "Submit";
            this.submitDiagnosisButton.UseVisualStyleBackColor = true;
            this.submitDiagnosisButton.Click += new System.EventHandler(this.submitDiagnosisButton_Click);
            // 
            // cancelDiagnosisButton
            // 
            this.cancelDiagnosisButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelDiagnosisButton.Location = new System.Drawing.Point(15, 155);
            this.cancelDiagnosisButton.Name = "cancelDiagnosisButton";
            this.cancelDiagnosisButton.Size = new System.Drawing.Size(75, 23);
            this.cancelDiagnosisButton.TabIndex = 6;
            this.cancelDiagnosisButton.Text = "Cancel";
            this.cancelDiagnosisButton.UseVisualStyleBackColor = true;
            this.cancelDiagnosisButton.Click += new System.EventHandler(this.cancelDiagnosisButton_Click);
            // 
            // DiagnosisForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 196);
            this.Controls.Add(this.cancelDiagnosisButton);
            this.Controls.Add(this.submitDiagnosisButton);
            this.Controls.Add(this.diagnosisRichTextBox);
            this.Controls.Add(this.diagnosisLabel);
            this.Name = "DiagnosisForm";
            this.Text = "DiagnosisForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label diagnosisLabel;
        private System.Windows.Forms.RichTextBox diagnosisRichTextBox;
        private System.Windows.Forms.Button submitDiagnosisButton;
        private System.Windows.Forms.Button cancelDiagnosisButton;
    }
}