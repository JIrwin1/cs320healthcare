﻿using System;
using System.Windows.Forms;
using HealthCareApplication.Controller;
using HealthCareApplication.Properties;

namespace HealthCareApplication.View
{
    /// <summary>
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class UpdateTestResultsForm : Form
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the update test results identifier.
        /// </summary>
        /// <value>
        ///     The update test results identifier.
        /// </value>
        public static int UpdateTestResultsId { get; set; }

        /// <summary>
        /// Gets or sets the update rout check identifier.
        /// </summary>
        /// <value>
        /// The update rout check identifier.
        /// </value>
        public static int UpdateRoutCheckId { get; set; }

    

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="UpdateTestResultsForm" /> class.
        /// </summary>
        public UpdateTestResultsForm()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        private void cancelTestResultsButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void submitTestResultsButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.resultsRichTextBox.Text == string.Empty)
                {
                    MessageBox.Show(
                        Resources
                            .UpdateTestResultsForm_submitTestResultsButton_Click_A_result_must_be_entered_in_order_to_be_submitted_);
                }
                else
                {
                    var resultsController = new LabResultController();
                   
                    resultsController.UpdateTestResults(UpdateTestResultsId, this.resultsRichTextBox.Text);
                    MessageBox.Show(
                        Resources
                            .UpdateTestResultsForm_submitTestResultsButton_Click_The_test_results_have_been_updated_);

                   
                    Close();
                }
            }
            catch (SystemException ecp)
            {
                MessageBox.Show(string.Format(Resources.AdminForm_sqlSubmitButton_Click_An_error_occurred___0_,
                    ecp.Message));
            }
        }

        #endregion
    }
}