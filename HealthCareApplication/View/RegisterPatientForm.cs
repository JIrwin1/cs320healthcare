﻿using System;
using System.Globalization;
using System.Windows.Forms;
using HealthCareApplication.Controller;
using HealthCareApplication.Model;
using HealthCareApplication.Properties;

namespace HealthCareApplication.View
{
    /// <summary>
    ///     Creates the Register Patient Form class
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class RegisterPatientForm : Form
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="RegisterPatientForm" /> class.
        /// </summary>
        public RegisterPatientForm()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        private void registerPatientCancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void registerPatientOkButton_Click(object sender, EventArgs e)
        {
            try
            {
                if ((this.fNameTextBox.Text == string.Empty) || (this.lNameTextBox.Text == string.Empty) ||
                    (this.street1TextBox.Text == string.Empty) || !this.contactMaskedTextBox1.MaskCompleted ||
                    (this.zipTextBox.Text == string.Empty) ||
                    (this.stateTextBox.Text == string.Empty) || (this.cityTextBox.Text == string.Empty))
                {
                    MessageBox.Show(
                        Resources
                            .RegisterPatientForm_registerPatientOkButton_Click_All_entry_fields_must_be_contain_the_appropriate_data_);
                }
                else
                {
                    var contact = this.contactMaskedTextBox1.Text;
                    if (this.street2TextBox.Text == string.Empty)
                    {
                        this.street2TextBox.Text = null;
                    }
                    var patientAddress = new Address(this.street1TextBox.Text, this.street2TextBox.Text,
                        this.cityTextBox.Text, this.stateTextBox.Text, this.zipTextBox.Text);

                    contact = contact.Replace(" ", "");

                    var controller = new PatientController();
                    controller.CreatePatient(this.fNameTextBox.Text, this.lNameTextBox.Text, patientAddress,
                        this.dateTimePicker1.Value.ToString(CultureInfo.InvariantCulture),
                        contact);
                    MessageBox.Show(this.fNameTextBox.Text +
                                    Resources.RegisterPatientForm_registerPatientOkButton_Click__ +
                                    this.lNameTextBox.Text +
                                    Resources
                                        .RegisterPatientForm_registerPatientOkButton_Click__has_been_register_as_a_patient_);
                    Close();
                }
            }
            catch (SystemException ecp)
            {
                MessageBox.Show(string.Format(Resources.AdminForm_sqlSubmitButton_Click_An_error_occurred___0_,
                    ecp.Message));
            }
        }

        #endregion
    }
}