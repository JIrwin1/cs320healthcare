﻿namespace HealthCareApplication.View
{
    partial class RoutineCheckForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bodyTempMaskedBox = new System.Windows.Forms.MaskedTextBox();
            this.bloodPresureMaskedBox = new System.Windows.Forms.MaskedTextBox();
            this.pulseMaskedBox = new System.Windows.Forms.MaskedTextBox();
            this.bloodPresureLabel = new System.Windows.Forms.Label();
            this.bodyTempLabel = new System.Windows.Forms.Label();
            this.pulseLabel = new System.Windows.Forms.Label();
            this.symptombsLabel = new System.Windows.Forms.Label();
            this.submitRoutineChecksButton = new System.Windows.Forms.Button();
            this.cancelRoutineChecksButton = new System.Windows.Forms.Button();
            this.symptomsTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // bodyTempMaskedBox
            // 
            this.bodyTempMaskedBox.Location = new System.Drawing.Point(132, 34);
            this.bodyTempMaskedBox.Mask = "999.99";
            this.bodyTempMaskedBox.Name = "bodyTempMaskedBox";
            this.bodyTempMaskedBox.Size = new System.Drawing.Size(100, 20);
            this.bodyTempMaskedBox.TabIndex = 0;
            // 
            // bloodPresureMaskedBox
            // 
            this.bloodPresureMaskedBox.Location = new System.Drawing.Point(132, 72);
            this.bloodPresureMaskedBox.Mask = "999 over 999";
            this.bloodPresureMaskedBox.Name = "bloodPresureMaskedBox";
            this.bloodPresureMaskedBox.Size = new System.Drawing.Size(100, 20);
            this.bloodPresureMaskedBox.TabIndex = 1;
            // 
            // pulseMaskedBox
            // 
            this.pulseMaskedBox.Location = new System.Drawing.Point(132, 104);
            this.pulseMaskedBox.Mask = "999 bpm";
            this.pulseMaskedBox.Name = "pulseMaskedBox";
            this.pulseMaskedBox.Size = new System.Drawing.Size(100, 20);
            this.pulseMaskedBox.TabIndex = 2;
            // 
            // bloodPresureLabel
            // 
            this.bloodPresureLabel.AutoSize = true;
            this.bloodPresureLabel.Location = new System.Drawing.Point(25, 79);
            this.bloodPresureLabel.Name = "bloodPresureLabel";
            this.bloodPresureLabel.Size = new System.Drawing.Size(81, 13);
            this.bloodPresureLabel.TabIndex = 3;
            this.bloodPresureLabel.Text = "Blood Pressure:";
            // 
            // bodyTempLabel
            // 
            this.bodyTempLabel.AutoSize = true;
            this.bodyTempLabel.Location = new System.Drawing.Point(25, 41);
            this.bodyTempLabel.Name = "bodyTempLabel";
            this.bodyTempLabel.Size = new System.Drawing.Size(91, 13);
            this.bodyTempLabel.TabIndex = 4;
            this.bodyTempLabel.Text = "Body Temprature:";
            // 
            // pulseLabel
            // 
            this.pulseLabel.AutoSize = true;
            this.pulseLabel.Location = new System.Drawing.Point(70, 111);
            this.pulseLabel.Name = "pulseLabel";
            this.pulseLabel.Size = new System.Drawing.Size(36, 13);
            this.pulseLabel.TabIndex = 5;
            this.pulseLabel.Text = "Pulse:";
            // 
            // symptombsLabel
            // 
            this.symptombsLabel.AutoSize = true;
            this.symptombsLabel.Location = new System.Drawing.Point(44, 146);
            this.symptombsLabel.Name = "symptombsLabel";
            this.symptombsLabel.Size = new System.Drawing.Size(58, 13);
            this.symptombsLabel.TabIndex = 7;
            this.symptombsLabel.Text = "Symptoms:";
            // 
            // submitRoutineChecksButton
            // 
            this.submitRoutineChecksButton.Location = new System.Drawing.Point(157, 265);
            this.submitRoutineChecksButton.Name = "submitRoutineChecksButton";
            this.submitRoutineChecksButton.Size = new System.Drawing.Size(75, 23);
            this.submitRoutineChecksButton.TabIndex = 8;
            this.submitRoutineChecksButton.Text = "Submit";
            this.submitRoutineChecksButton.UseVisualStyleBackColor = true;
            this.submitRoutineChecksButton.Click += new System.EventHandler(this.submitRoutineChecksButton_Click);
            // 
            // cancelRoutineChecksButton
            // 
            this.cancelRoutineChecksButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelRoutineChecksButton.Location = new System.Drawing.Point(28, 265);
            this.cancelRoutineChecksButton.Name = "cancelRoutineChecksButton";
            this.cancelRoutineChecksButton.Size = new System.Drawing.Size(75, 23);
            this.cancelRoutineChecksButton.TabIndex = 9;
            this.cancelRoutineChecksButton.Text = "Cancel";
            this.cancelRoutineChecksButton.UseVisualStyleBackColor = true;
            // 
            // symptomsTextBox
            // 
            this.symptomsTextBox.Location = new System.Drawing.Point(32, 162);
            this.symptomsTextBox.MaxLength = 45;
            this.symptomsTextBox.Name = "symptomsTextBox";
            this.symptomsTextBox.Size = new System.Drawing.Size(200, 72);
            this.symptomsTextBox.TabIndex = 13;
            this.symptomsTextBox.Text = "";
            // 
            // RoutineCheckForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(247, 314);
            this.Controls.Add(this.symptomsTextBox);
            this.Controls.Add(this.cancelRoutineChecksButton);
            this.Controls.Add(this.submitRoutineChecksButton);
            this.Controls.Add(this.symptombsLabel);
            this.Controls.Add(this.pulseLabel);
            this.Controls.Add(this.bodyTempLabel);
            this.Controls.Add(this.bloodPresureLabel);
            this.Controls.Add(this.pulseMaskedBox);
            this.Controls.Add(this.bloodPresureMaskedBox);
            this.Controls.Add(this.bodyTempMaskedBox);
            this.Name = "RoutineCheckForm";
            this.Text = "RoutineChecks";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox bodyTempMaskedBox;
        private System.Windows.Forms.MaskedTextBox bloodPresureMaskedBox;
        private System.Windows.Forms.MaskedTextBox pulseMaskedBox;
        private System.Windows.Forms.Label bloodPresureLabel;
        private System.Windows.Forms.Label bodyTempLabel;
        private System.Windows.Forms.Label pulseLabel;
        private System.Windows.Forms.Label symptombsLabel;
        private System.Windows.Forms.Button submitRoutineChecksButton;
        private System.Windows.Forms.Button cancelRoutineChecksButton;
        private System.Windows.Forms.RichTextBox symptomsTextBox;
    }

}
