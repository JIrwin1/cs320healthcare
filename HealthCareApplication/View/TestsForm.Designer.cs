﻿namespace HealthCareApplication.View
{
    partial class TestsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.testToAddLabel = new System.Windows.Forms.Label();
            this.testAddTestbutton = new System.Windows.Forms.Button();
            this.testCancelButton = new System.Windows.Forms.Button();
            this.scheduleTestDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.shceduleTestDateLabel = new System.Windows.Forms.Label();
            this.testNameComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // testToAddLabel
            // 
            this.testToAddLabel.AutoSize = true;
            this.testToAddLabel.Location = new System.Drawing.Point(40, 36);
            this.testToAddLabel.Name = "testToAddLabel";
            this.testToAddLabel.Size = new System.Drawing.Size(64, 13);
            this.testToAddLabel.TabIndex = 1;
            this.testToAddLabel.Text = "Test to add:";
            // 
            // testAddTestbutton
            // 
            this.testAddTestbutton.Location = new System.Drawing.Point(126, 184);
            this.testAddTestbutton.Name = "testAddTestbutton";
            this.testAddTestbutton.Size = new System.Drawing.Size(75, 23);
            this.testAddTestbutton.TabIndex = 2;
            this.testAddTestbutton.Text = "Add Test";
            this.testAddTestbutton.UseVisualStyleBackColor = true;
            this.testAddTestbutton.Click += new System.EventHandler(this.testAddTestbutton_Click);
            // 
            // testCancelButton
            // 
            this.testCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.testCancelButton.Location = new System.Drawing.Point(12, 184);
            this.testCancelButton.Name = "testCancelButton";
            this.testCancelButton.Size = new System.Drawing.Size(75, 23);
            this.testCancelButton.TabIndex = 3;
            this.testCancelButton.Text = "Cancel";
            this.testCancelButton.UseVisualStyleBackColor = true;
            this.testCancelButton.Click += new System.EventHandler(this.testCancelButton_Click);
            // 
            // scheduleTestDateTimePicker
            // 
            this.scheduleTestDateTimePicker.CustomFormat = "MM-dd-yyyy";
            this.scheduleTestDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.scheduleTestDateTimePicker.Location = new System.Drawing.Point(43, 136);
            this.scheduleTestDateTimePicker.Name = "scheduleTestDateTimePicker";
            this.scheduleTestDateTimePicker.Size = new System.Drawing.Size(100, 20);
            this.scheduleTestDateTimePicker.TabIndex = 4;
            // 
            // shceduleTestDateLabel
            // 
            this.shceduleTestDateLabel.AutoSize = true;
            this.shceduleTestDateLabel.Location = new System.Drawing.Point(40, 109);
            this.shceduleTestDateLabel.Name = "shceduleTestDateLabel";
            this.shceduleTestDateLabel.Size = new System.Drawing.Size(90, 13);
            this.shceduleTestDateLabel.TabIndex = 5;
            this.shceduleTestDateLabel.Text = "Schedule test on:";
            // 
            // testNameComboBox
            // 
            this.testNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.testNameComboBox.FormattingEnabled = true;
            this.testNameComboBox.Location = new System.Drawing.Point(43, 64);
            this.testNameComboBox.Name = "testNameComboBox";
            this.testNameComboBox.Size = new System.Drawing.Size(158, 21);
            this.testNameComboBox.TabIndex = 6;
            // 
            // TestsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(213, 219);
            this.Controls.Add(this.testNameComboBox);
            this.Controls.Add(this.shceduleTestDateLabel);
            this.Controls.Add(this.scheduleTestDateTimePicker);
            this.Controls.Add(this.testCancelButton);
            this.Controls.Add(this.testAddTestbutton);
            this.Controls.Add(this.testToAddLabel);
            this.Name = "TestsForm";
            this.Text = "TestsForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label testToAddLabel;
        private System.Windows.Forms.Button testAddTestbutton;
        private System.Windows.Forms.Button testCancelButton;
        private System.Windows.Forms.DateTimePicker scheduleTestDateTimePicker;
        private System.Windows.Forms.Label shceduleTestDateLabel;
        private System.Windows.Forms.ComboBox testNameComboBox;
    }
}