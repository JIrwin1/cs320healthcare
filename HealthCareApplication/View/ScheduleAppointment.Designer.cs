﻿using System.Windows.Forms;

namespace HealthCareApplication.View
{
    partial class ScheduleAppointment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateLabel = new System.Windows.Forms.Label();
            this.reasonLabel = new System.Windows.Forms.Label();
            this.doctorLabel = new System.Windows.Forms.Label();
            this.scheduleAppointmentDatePicker = new System.Windows.Forms.DateTimePicker();
            this.reasonForAppointmentTextBox = new System.Windows.Forms.TextBox();
            this.scheduleAppointmentSubmitButton = new System.Windows.Forms.Button();
            this.scheduleAppointmentCancelButton = new System.Windows.Forms.Button();
            this.doctorNameCombobox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // dateLabel
            // 
            this.dateLabel.AutoSize = true;
            this.dateLabel.Location = new System.Drawing.Point(12, 30);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(61, 13);
            this.dateLabel.TabIndex = 1;
            this.dateLabel.Text = "Date/Time:";
            // 
            // reasonLabel
            // 
            this.reasonLabel.AutoSize = true;
            this.reasonLabel.Location = new System.Drawing.Point(1, 61);
            this.reasonLabel.Name = "reasonLabel";
            this.reasonLabel.Size = new System.Drawing.Size(121, 13);
            this.reasonLabel.TabIndex = 3;
            this.reasonLabel.Text = "Reason for Appointment";
            // 
            // doctorLabel
            // 
            this.doctorLabel.AutoSize = true;
            this.doctorLabel.Location = new System.Drawing.Point(66, 93);
            this.doctorLabel.Name = "doctorLabel";
            this.doctorLabel.Size = new System.Drawing.Size(42, 13);
            this.doctorLabel.TabIndex = 5;
            this.doctorLabel.Text = "Doctor:";
            // 
            // scheduleAppointmentDatePicker
            // 
            this.scheduleAppointmentDatePicker.CustomFormat = "dd-MM-yyyy hh:mm:ss tt";
            this.scheduleAppointmentDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.scheduleAppointmentDatePicker.Location = new System.Drawing.Point(93, 23);
            this.scheduleAppointmentDatePicker.Name = "scheduleAppointmentDatePicker";
            this.scheduleAppointmentDatePicker.Size = new System.Drawing.Size(157, 20);
            this.scheduleAppointmentDatePicker.TabIndex = 7;
            // 
            // reasonForAppointmentTextBox
            // 
            this.reasonForAppointmentTextBox.Location = new System.Drawing.Point(150, 54);
            this.reasonForAppointmentTextBox.Name = "reasonForAppointmentTextBox";
            this.reasonForAppointmentTextBox.Size = new System.Drawing.Size(100, 20);
            this.reasonForAppointmentTextBox.TabIndex = 9;
            // 
            // scheduleAppointmentSubmitButton
            // 
            this.scheduleAppointmentSubmitButton.Location = new System.Drawing.Point(175, 161);
            this.scheduleAppointmentSubmitButton.Name = "scheduleAppointmentSubmitButton";
            this.scheduleAppointmentSubmitButton.Size = new System.Drawing.Size(75, 23);
            this.scheduleAppointmentSubmitButton.TabIndex = 13;
            this.scheduleAppointmentSubmitButton.Text = "Submit";
            this.scheduleAppointmentSubmitButton.UseVisualStyleBackColor = true;
            this.scheduleAppointmentSubmitButton.Click += new System.EventHandler(this.scheduleAppointmentSubmitButton_Click);
            // 
            // scheduleAppointmentCancelButton
            // 
            this.scheduleAppointmentCancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.scheduleAppointmentCancelButton.Location = new System.Drawing.Point(4, 161);
            this.scheduleAppointmentCancelButton.Name = "scheduleAppointmentCancelButton";
            this.scheduleAppointmentCancelButton.Size = new System.Drawing.Size(75, 23);
            this.scheduleAppointmentCancelButton.TabIndex = 14;
            this.scheduleAppointmentCancelButton.Text = "Cancel";
            this.scheduleAppointmentCancelButton.UseVisualStyleBackColor = true;
            // 
            // doctorNameCombobox
            // 
            this.doctorNameCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.doctorNameCombobox.FormattingEnabled = true;
            this.doctorNameCombobox.Location = new System.Drawing.Point(150, 85);
            this.doctorNameCombobox.Name = "doctorNameCombobox";
            this.doctorNameCombobox.Size = new System.Drawing.Size(100, 21);
            this.doctorNameCombobox.TabIndex = 15;
            // 
            // ScheduleAppointment
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(259, 191);
            this.Controls.Add(this.doctorNameCombobox);
            this.Controls.Add(this.scheduleAppointmentCancelButton);
            this.Controls.Add(this.scheduleAppointmentSubmitButton);
            this.Controls.Add(this.reasonForAppointmentTextBox);
            this.Controls.Add(this.scheduleAppointmentDatePicker);
            this.Controls.Add(this.doctorLabel);
            this.Controls.Add(this.reasonLabel);
            this.Controls.Add(this.dateLabel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ScheduleAppointment";
            this.Text = "ScheduleAppointment";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label dateLabel;
        private System.Windows.Forms.Label reasonLabel;
        private System.Windows.Forms.Label doctorLabel;
        private System.Windows.Forms.DateTimePicker scheduleAppointmentDatePicker;
        private TextBox reasonForAppointmentTextBox;
        private Button scheduleAppointmentSubmitButton;
        private Button scheduleAppointmentCancelButton;
        private ComboBox doctorNameCombobox;
    }
}