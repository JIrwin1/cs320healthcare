﻿using System;
using System.Windows.Forms;
using HealthCareApplication.Controller;
using HealthCareApplication.Properties;

namespace HealthCareApplication.View
{
    /// <summary>
    ///     Creates the Routine Check Form
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class RoutineCheckForm : Form
    {
        #region Data members

        private readonly RoutineChecksController checkController = new RoutineChecksController();
        private readonly AppointmentController appController = new AppointmentController();

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the nurse identifier.
        /// </summary>
        /// <value>
        ///     The nurse identifier.
        /// </value>
        public static int NurseId { get; set; }

        /// <summary>
        ///     Gets or sets the r check appointment identifier.
        /// </summary>
        /// <value>
        ///     The r check appointment identifier.
        /// </value>
        public static int RCheckAppointmentId { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="RoutineCheckForm" /> class.
        /// </summary>
        public RoutineCheckForm()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        private void submitRoutineChecksButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (!this.bodyTempMaskedBox.MaskFull)
                {
                    MessageBox.Show(
                        Resources
                            .RoutineCheckForm_submitRoutineChecksButton_Click_The_body_temprature_should_be_in_the_format_XXX_XX);
                }
                else if (!this.bloodPresureMaskedBox.MaskFull)
                {
                    MessageBox.Show(
                        Resources
                            .RoutineCheckForm_submitRoutineChecksButton_Click_The_blood_presure_should_be_in_the_format_XXX_over_XXX);
                }
                else if (!this.pulseMaskedBox.MaskFull)
                {
                    MessageBox.Show(
                        Resources
                            .RoutineCheckForm_submitRoutineChecksButton_Click_The_pulse_should_be_in_the_format_XXX_bpm);
                }
                else if (this.symptomsTextBox.Text == string.Empty)
                {
                    MessageBox.Show(
                        Resources
                            .RoutineCheckForm_submitRoutineChecksButton_Click_All_fields_must_be_entered_filled_out_fully_);
                }

                else
                {
                    this.checkController.AddRoutineChecks(Convert.ToDecimal(this.bodyTempMaskedBox.Text),
                            this.bloodPresureMaskedBox.Text, this.pulseMaskedBox.Text, this.symptomsTextBox.Text,
                            NurseId);
                    var lastCheckMade = this.checkController.GetIdOfLastCheckMade();
                    this.appController.UpdateTheAppointmentWithACheckId(RCheckAppointmentId, lastCheckMade);

                    MessageBox.Show(
                        Resources.RoutineCheckForm_submitRoutineChecksButton_Click_Routine_Checks_has_been_added_);
                    Close();
                }
            }
            catch (SystemException ecp)
            {
                MessageBox.Show(string.Format(Resources.AdminForm_sqlSubmitButton_Click_An_error_occurred___0_,
                    ecp.Message));
            }
        }

        #endregion
    }
}