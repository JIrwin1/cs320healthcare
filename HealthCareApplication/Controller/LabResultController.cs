﻿using System;
using System.Collections.Generic;
using HealthCareApplication.DAL.Repository;
using HealthCareApplication.Model;

namespace HealthCareApplication.Controller
{
    internal class LabResultController
    {
        #region Data members

        private readonly LabResultsRepository resultsRepo;
        private readonly AppointmentController appRepo;

        #endregion

        #region Constructors

        public LabResultController()
        {
            this.resultsRepo = new LabResultsRepository();
            this.appRepo = new AppointmentController();
        }

        #endregion

        #region Methods

        public void AddResultToTable(int routcheckid, int type, string result, DateTime performedDate)
        {
            var theTest = new LabResults(type, result, performedDate);
            this.resultsRepo.Add(theTest);

            var idOfAppointment = this.appRepo.GetAppointmentByCheckId(routcheckid).AppointmentId;
            var lastTestMade = this.resultsRepo.GetLastMadeCheckId();

            this.resultsRepo.AddToInBetweenTableForResultAndAppointment(idOfAppointment, lastTestMade);
        }

        public List<LabResults> GetTestForPatient(Patient thePatient)
        {
            return this.resultsRepo.GetPatientTests(thePatient);
        }

        public void UpdateTestResults(int testId, string testResults)
        {
            this.resultsRepo.UpdateResultOfTest(testId, testResults);
        }

        public Dictionary<int, string> GetTestTypes()
        {
            return this.resultsRepo.GetTestTypes();
        }

        public Boolean DoesLabHaveNoResult(int labId)
        {
            return this.resultsRepo.CheckToSeeIfLabResultHasANotResults(labId);
        }

        #endregion
    }
}