﻿using HealthCareApplication.DAL.Repository;
using HealthCareApplication.Model;

namespace HealthCareApplication.Controller
{
    internal class AdminController
    {
        #region Data members

        private readonly AdminRepository adminRepo;

        #endregion

        #region Constructors

        public AdminController()
        {
            this.adminRepo = new AdminRepository();
        }

        #endregion

        #region Methods

        public bool CheckIfAdminIsLoggedInCorrectly(string loginId, string password)
        {
            var admin = this.adminRepo.GetAdminByLoginId(loginId);

            if (admin == null)
            {
                return false;
            }

            var isAdminLoggedInCorrectly = this.adminRepo.CheckToSeeIfCorrectPasswordIsBeingUsed(password, admin);

            return isAdminLoggedInCorrectly;
        }

        public Admin GetAdminWithLoginUserNameInformation(string loginId)
        {
            var admin = this.adminRepo.GetAdminByLoginId(loginId);
            return admin;
        }

        public void AdminPassInSqlCommand(string theSqlCommand)
        {
            this.adminRepo.ImputSqlCommandForAdmin(theSqlCommand);
        }

        #endregion
    }
}