﻿using System.Collections.Generic;
using HealthCareApplication.DAL.Repository;
using HealthCareApplication.Model;

namespace HealthCareApplication.Controller
{
    /// <summary>
    ///     Diagnosis Controller
    /// </summary>
    public class DiagnosisController
    {
        #region Data members

        private readonly DiagnosisRepository diagnosisRepo;
        private readonly AppointmentController appCont;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="DiagnosisController" /> class.
        /// </summary>
        public DiagnosisController()
        {
            this.diagnosisRepo = new DiagnosisRepository();
            this.appCont = new AppointmentController();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Adds the diagnosis.
        /// </summary>
        /// <param name="description">The description.</param>
        /// <param name="routineCheckId">The routine check identifier.</param>
        /// <param name="finalDiagnosis">The final Diagnosis</param>
        public void AddDiagnosis(string description, int routineCheckId, string finalDiagnosis)
        {
            var appointmentId = this.appCont.GetAppointmentByCheckId(routineCheckId).AppointmentId;
            var diagnosis = new Diagnosis(description, appointmentId, finalDiagnosis);
            this.diagnosisRepo.Add(diagnosis);
        }

        /// <summary>
        ///     Gets the diagnoses.
        /// </summary>
        /// <param name="thePatient">The patient.</param>
        /// <returns></returns>
        public List<Diagnosis> GetDiagnoses(Patient thePatient)
        {
            return this.diagnosisRepo.GetDiagnosesInformationForPatient(thePatient);
        }

        /// <summary>
        ///     Updates the dio results.
        /// </summary>
        /// <param name="dioId">The dio identifier.</param>
        /// <param name="resultString">The result string.</param>
        public void UpdateDioResults(int dioId, string resultString)
        {
            this.diagnosisRepo.UpdateDioResult(dioId, resultString);
        }

        /// <summary>
        ///     Doeses the dio already exist for given appointment.
        /// </summary>
        /// <param name="checkId">The check identifier.</param>
        /// <returns>t/r if a diagnosis already exists</returns>
        public bool DoesDioAlreadyExistForGivenAppointment(int checkId)
        {
            var appointmentId = this.appCont.GetAppointmentByCheckId(checkId).AppointmentId;
            return this.diagnosisRepo.CheckToSeeIfDioExists(appointmentId);
        }

        public bool CheckToSeeIfDioDoesntHaveAFinalDio(int id) {
            return this.diagnosisRepo.CheckToSeeIfDioDoesntHaveAFinalDio(id);
        }

        #endregion
    }
}