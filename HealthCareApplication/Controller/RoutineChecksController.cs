﻿using System.Collections.Generic;
using HealthCareApplication.DAL.Repository;
using HealthCareApplication.Model;

namespace HealthCareApplication.Controller
{
    internal class RoutineChecksController
    {
        #region Data members

        private readonly RoutineChecksRepository checksRepo;

        #endregion

        #region Constructors

        public RoutineChecksController()
        {
            this.checksRepo = new RoutineChecksRepository();
        }

        #endregion

        #region Methods

        public void AddRoutineChecks(decimal bodytemp, string bloodPressure, string pulse, string symptoms, int nurse)
        {
            var routineChecks = new RoutineChecks(bodytemp, bloodPressure, pulse, symptoms, nurse);
            this.checksRepo.Add(routineChecks);
        }

        public List<RoutineChecks> GetRoutineChecksForPatient(Patient thePatient)
        {
            return this.checksRepo.GetRoutineChecksOfPatient(thePatient);
        }

        public bool DoesRoutineCheckExist(int appointmentId)
        {
            return this.checksRepo.CheckToSeeIfCheckExists(appointmentId);
        }

        public int GetIdOfLastCheckMade()
        {
            return this.checksRepo.GetLastMadeCheckId();
        }

        public RoutineChecks GetRoutCheck(int id)
        {
            return this.checksRepo.GetByld(id);
        }

        #endregion
    }
}