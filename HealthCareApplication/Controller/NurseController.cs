﻿using HealthCareApplication.DAL.Repository;
using HealthCareApplication.Model;

namespace HealthCareApplication.Controller
{
    internal class NurseController
    {
        #region Data members

        private readonly NurseRepository nurseRepo;

        #endregion

        #region Constructors

        public NurseController()
        {
            this.nurseRepo = new NurseRepository();
        }

        #endregion

        #region Methods

        public bool CheckIfNurseIsLoggedInCorrectly(string loginId, string thepassword)
        {
            var nurse = this.nurseRepo.GetNurseByLoginId(loginId);

            if (nurse == null)
            {
                return false;
            }

            var loggedInCorrectly = this.nurseRepo.CheckToSeeIfCorrectPasswordIsBeingUsed(thepassword,
                                            nurse);
            return loggedInCorrectly;
        }

        public Nurse GetNurseFromNurseIdForLogin(string loginId)
        {
            var nurse = this.nurseRepo.GetNurseByLoginId(loginId);

            return nurse;
        }

        public Nurse GetNurseById(int id)
        {
            return this.nurseRepo.GetByld(id);
        }

        #endregion
    }
}