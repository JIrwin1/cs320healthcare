﻿using System.Collections.Generic;
using HealthCareApplication.DAL.Repository;
using HealthCareApplication.Model;

namespace HealthCareApplication.Controller
{
    internal class DoctorController
    {
        #region Data members

        private readonly DoctorRepository docRepo;

        #endregion

        #region Constructors

        public DoctorController()
        {
            this.docRepo = new DoctorRepository();
        }

        #endregion

        #region Methods

        public bool DoesDoctorExist(int doctorId)
        {
            return this.docRepo.CheckToSeeIfDocotorExists(doctorId);
        }

        public List<Doctor> GetAllDoctors()
        {
            return (List<Doctor>) this.docRepo.GetAll();
        }

        public Doctor GetDoctorWithId(int id)
        {
            return this.docRepo.GetByld(id);
        }

        #endregion
    }
}