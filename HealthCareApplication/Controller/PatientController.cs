﻿using System;
using System.Collections.Generic;
using HealthCareApplication.DAL.Repository;
using HealthCareApplication.Model;

namespace HealthCareApplication.Controller
{
    internal class PatientController
    {
        #region Data members

        private readonly PatientReposiotry patRepo;

        #endregion

        #region Constructors

        public PatientController()
        {
            this.patRepo = new PatientReposiotry();
        }

        #endregion

        #region Methods

        public void CreatePatient(string firstName, string lName, Address address, string dateOfBIrth,
            string contactPhoneNumber)
        {
            var newPatient = new Patient(firstName, lName, address, dateOfBIrth, contactPhoneNumber);
            this.patRepo.Add(newPatient);
        }

        public bool CheckIfPatientExists(int id)
        {
            return this.patRepo.CheckIfPatientExists(id);
        }

        public List<Patient> GetPatientInformationFromSearchByFirstName(string firstName)
        {
            return this.patRepo.GetPatientsFromGivenInformationbyFirstName(firstName);
        }

        public List<Patient> GetPatientInformationFromSearchByLastName(string lastName)
        {
            return this.patRepo.GetPatientsFromGivenInformationByLastName(lastName);
        }

        public List<Patient> GetPatientInformationFromSearchByDob(DateTime date)
        {
            return this.patRepo.GetPatientsFromGivenInformationByDob(date);
        }

        public List<Patient> GetPatientInformationFromSearchByFirstLastName(string firstName, string lastName)
        {
            return this.patRepo.GetPatientsFromGivenInformationByFirstLastName(firstName, lastName);
        }

        public List<Patient> GetPatientInformationFromSearchByFirstNameDob(string firstName, DateTime date)
        {
            return this.patRepo.GetPatientsFromGivenInformationByFirstNameDob(firstName, date);
        }

        public List<Patient> GetPatientInformationFromSearchByLastNameDob(string lastName, DateTime date)
        {
            return this.patRepo.GetPatientsFromGivenInformationByLastNameDob(lastName, date);
        }

        public List<Patient> GetPatientInformationFromSearchByFirstLastNameDob(string firstName, string lastName,
            DateTime date)
        {
            return this.patRepo.GetPatientsFromGivenInformationByFirstLastNameDob(firstName, lastName, date);
        }

        public Patient GetPatientById(int id)
        {
            return this.patRepo.GetByld(id);
        }

        #endregion
    }
}