﻿using System;
using System.Collections.Generic;
using HealthCareApplication.DAL.Repository;
using HealthCareApplication.Model;

namespace HealthCareApplication.Controller
{
    internal class AppointmentController
    {
        #region Data members

        private readonly Appointmentrepository appRepo;
        private readonly RoutineChecksController routCont;
        private readonly DoctorController docCont;
        private readonly NurseController nurseCont;
        private readonly PatientController patCont;

        #endregion

        #region Constructors

        public AppointmentController()
        {
            this.appRepo = new Appointmentrepository();
            this.routCont = new RoutineChecksController();
            this.docCont = new DoctorController();
            this.nurseCont = new NurseController();
            this.patCont = new PatientController();
        }

        #endregion

        #region Methods

        public void AddAppointment(DateTime time, string reason, int? checkid, int doctorid, int patientid)
        {
            var theAppointment = new Appointment(time, reason, checkid, doctorid, patientid);
            this.appRepo.Add(theAppointment);
        }

        public List<Appointment> GetAppointsOfPatient(Patient thePatient)
        {
            return this.appRepo.GetAppointInformationForPatient(thePatient);
        }

        public void UpdateTheAppointmentWithACheckId(int appointmentId, int checkid)
        {
            this.appRepo.SetTHeLastMadeCheckToAppointmentWithId(appointmentId, checkid);
        }

        public List<VistInformation> GetVistInformationBetweenThedates(DateTime startDate, DateTime endDate)
        {
            var theAppointments = this.appRepo.GetAppointmentsBetweenDates(startDate, endDate);
            var allVistestBetweenDates = new List<VistInformation>();

            foreach (var appointment in theAppointments)
            {
                var nurseId = this.routCont.GetRoutCheck(Convert.ToInt32(appointment.RoutineCheckId)).NurseId;
                var patientId = appointment.PatientId;
                var doctorId = appointment.DoctorId;
                var vistDate = appointment.DateTime;

                var doctorName = this.docCont.GetDoctorWithId(doctorId).Fname + " " +
                                 this.docCont.GetDoctorWithId(doctorId).Lname;
                var nurseName = this.nurseCont.GetNurseById(nurseId).FName + " " +
                                this.nurseCont.GetNurseById(nurseId).LName;
                var patientName = this.patCont.GetPatientById(patientId).FName + " " +
                                  this.patCont.GetPatientById(patientId).LName;

                var vistInfo = new VistInformation(vistDate, patientId, patientName, doctorName, nurseName);

                allVistestBetweenDates.Add(vistInfo);
            }

            return allVistestBetweenDates;
        }

        public Appointment GetAppointmentByCheckId(int id)
        {
            return this.appRepo.GetAppointmentByCheckId(id);
        }

        #endregion
    }
}