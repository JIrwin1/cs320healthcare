﻿using System;

namespace HealthCareApplication.Model
{
    internal class LabResults
    {
        #region Data members

        private readonly int testTypeId;

        #endregion

        #region Properties

        public int TestId { get; set; }

        public string TestType { get; set; }

        public string Results { get; set; }

        public DateTime PerformOnDate { get; set; }

        #endregion

        #region Constructors

        public LabResults(int testId, string testType, string results, DateTime performedDate)
        {
            this.TestId = testId;
            this.TestType = testType;
            this.Results = results;
            this.PerformOnDate = performedDate;
        }

        public LabResults(int testType, string results, DateTime performedDate)
        {
            this.testTypeId = testType;
            this.Results = results;
            this.PerformOnDate = performedDate;
        }

        #endregion

        #region Methods

        public int GetTestId()
        {
            return this.testTypeId;
        }

        #endregion
    }
}