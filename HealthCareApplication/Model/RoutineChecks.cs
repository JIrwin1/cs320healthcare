﻿namespace HealthCareApplication.Model
{
    /// <summary>
    ///     Creates Routine checks class
    /// </summary>
    public class RoutineChecks
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the routine check identifier.
        /// </summary>
        /// <value>
        ///     The routine check identifier.
        /// </value>
        public int RoutineCheckId { get; set; }

        /// <summary>
        ///     Gets or sets the body temprature.
        /// </summary>
        /// <value>
        ///     The body temprature.
        /// </value>
        public decimal BodyTemprature { get; set; }

        /// <summary>
        ///     Gets or sets the blood presure.
        /// </summary>
        /// <value>
        ///     The blood presure.
        /// </value>
        public string BloodPresure { get; set; }

        /// <summary>
        ///     Gets or sets the pulse.
        /// </summary>
        /// <value>
        ///     The pulse.
        /// </value>
        public string Pulse { get; set; }

        /// <summary>
        ///     Gets or sets the symptoms.
        /// </summary>
        /// <value>
        ///     The symptoms.
        /// </value>
        public string Symptoms { get; set; }

        /// <summary>
        ///     Gets or sets the nurse identifier.
        /// </summary>
        /// <value>
        ///     The nurse identifier.
        /// </value>
        public int NurseId { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="RoutineChecks" /> class.
        /// </summary>
        /// <param name="routineId">The routine identifier.</param>
        /// <param name="bodyTemp">The body temporary.</param>
        /// <param name="bloodPressure">The blood pressure.</param>
        /// <param name="pulse">The pulse.</param>
        /// <param name="symptoms">The symptoms.</param>
        /// <param name="nurse">The nurse.</param>
        public RoutineChecks(int routineId, decimal bodyTemp, string bloodPressure, string pulse, string symptoms,
            int nurse)
        {
            this.RoutineCheckId = routineId;
            this.BodyTemprature = bodyTemp;
            this.BloodPresure = bloodPressure;
            this.Pulse = pulse;
            this.Symptoms = symptoms;
            this.NurseId = nurse;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="RoutineChecks" /> class.
        /// </summary>
        /// <param name="bodyTemp">The body temporary.</param>
        /// <param name="bloodPressure">The blood pressure.</param>
        /// <param name="pulse">The pulse.</param>
        /// <param name="symptoms">The symptoms.</param>
        /// <param name="nurse">The nurse.</param>
        public RoutineChecks(decimal bodyTemp, string bloodPressure, string pulse, string symptoms, int nurse)
        {
            this.BodyTemprature = bodyTemp;
            this.BloodPresure = bloodPressure;
            this.Pulse = pulse;
            this.Symptoms = symptoms;
            this.NurseId = nurse;
        }

        #endregion
    }
}