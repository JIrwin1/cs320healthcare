﻿using System;

namespace HealthCareApplication.Model
{
    /// <summary>
    ///     Creates appointment class
    /// </summary>
    public class Appointment
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the appointment identifier.
        /// </summary>
        /// <value>
        ///     The appointment identifier.
        /// </value>
        public int AppointmentId { get; set; }

        /// <summary>
        ///     Gets or sets the date time.
        /// </summary>
        /// <value>
        ///     The date time.
        /// </value>
        public DateTime DateTime { get; set; }

        /// <summary>
        ///     Gets or sets the reason for appointment.
        /// </summary>
        /// <value>
        ///     The reason for appointment.
        /// </value>
        public string ReasonForAppointment { get; set; }

        /// <summary>
        ///     Gets or sets the routine check identifier.
        /// </summary>
        /// <value>
        ///     The routine check identifier.
        /// </value>
        public int? RoutineCheckId { get; set; }

        /// <summary>
        ///     Gets or sets the doctor identifier.
        /// </summary>
        /// <value>
        ///     The doctor identifier.
        /// </value>
        public int DoctorId { get; set; }

        /// <summary>
        ///     Gets or sets the patient identifier.
        /// </summary>
        /// <value>
        ///     The patient identifier.
        /// </value>
        public int PatientId { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Appointment" /> class.
        /// </summary>
        /// <param name="appointmentId">The appointment identifier.</param>
        /// <param name="dateTime">The date time.</param>
        /// <param name="reasonForAppointment">The reason for appointment.</param>
        /// <param name="routineCheckId">The routine check identifier.</param>
        /// <param name="doctorId">The doctor identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        public Appointment(int appointmentId, DateTime dateTime, string reasonForAppointment, int? routineCheckId,
            int doctorId, int patientId)
        {
            this.AppointmentId = appointmentId;
            this.DateTime = dateTime;
            this.ReasonForAppointment = reasonForAppointment;
            this.RoutineCheckId = routineCheckId;
            this.DoctorId = doctorId;
            this.PatientId = patientId;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Appointment" /> class.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <param name="reasonForAppointment">The reason for appointment.</param>
        /// <param name="routineCheckId">The routine check identifier.</param>
        /// <param name="doctorId">The doctor identifier.</param>
        /// <param name="patientId">The patient identifier.</param>
        public Appointment(DateTime dateTime, string reasonForAppointment, int? routineCheckId, int doctorId,
            int patientId)
        {
            this.DateTime = dateTime;
            this.ReasonForAppointment = reasonForAppointment;
            this.RoutineCheckId = routineCheckId;
            this.DoctorId = doctorId;
            this.PatientId = patientId;
        }

        #endregion
    }
}