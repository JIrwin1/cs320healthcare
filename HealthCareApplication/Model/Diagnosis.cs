﻿namespace HealthCareApplication.Model
{
    /// <summary>
    ///     Creates Diagnosis class
    /// </summary>
    public class Diagnosis
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the diagnosis identifier.
        /// </summary>
        /// <value>
        ///     The diagnosis identifier.
        /// </value>
        public int DiagnosisId { get; set; }

        /// <summary>
        ///     Gets or sets the description.
        /// </summary>
        /// <value>
        ///     The description.
        /// </value>
        public string DiagnosisDescription { get; set; }

        /// <summary>
        ///     Gets or sets the appointments appointment identifier.
        /// </summary>
        /// <value>
        ///     The appointments appointment identifier.
        /// </value>
        public int AppointmentsAppointmentId { get; set; }

        /// <summary>
        /// Gets or sets the final diag.
        /// </summary>
        /// <value>
        /// The final diag.
        /// </value>
        public string FinalDiag { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Diagnosis" /> class.
        /// </summary>
        /// <param name="diagnosisId">The diagnosis identifier.</param>
        /// <param name="description">The description.</param>
        /// <param name="appId">The application identifier.</param>
        /// <param name="finalDiag">The final diagnosis</param>
        public Diagnosis(int diagnosisId, string description, int appId, string finalDiag)
        {
            this.DiagnosisId = diagnosisId;
            this.DiagnosisDescription = description;
            this.AppointmentsAppointmentId = appId;
            this.FinalDiag = finalDiag;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Diagnosis" /> class.
        /// </summary>
        /// <param name="description">The description.</param>
        /// <param name="appId">The application identifier.</param>
        /// <param name="finalDiag">The final diagnosis</param>
        public Diagnosis(string description, int appId, string finalDiag)
        {
            this.DiagnosisDescription = description;
            this.AppointmentsAppointmentId = appId;
            this.FinalDiag = finalDiag;
        }

        #endregion
    }
}