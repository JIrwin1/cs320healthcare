﻿using System;

namespace HealthCareApplication.Model
{
    internal class VistInformation
    {
        #region Properties

        public DateTime VistDate { get; set; }
        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public string DoctorName { get; set; }
        public string NurseName { get; set; }

        #endregion

        #region Constructors

        public VistInformation(DateTime vistTime, int patientId, string patientName, string doctorName, string nurseName)
        {
            this.VistDate = vistTime;
            this.PatientId = patientId;
            this.PatientName = patientName;
            this.DoctorName = doctorName;
            this.NurseName = nurseName;
        }

        #endregion
    }
}