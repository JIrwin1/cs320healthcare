﻿namespace HealthCareApplication.Model
{
    /// <summary>
    ///     Creates patient class
    /// </summary>
    public class Patient
    {
        #region Data members

        private readonly Address theAddress;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the patient identifier.
        /// </summary>
        /// <value>
        ///     The patient identifier.
        /// </value>
        public int PatientId { get; set; }

        /// <summary>
        ///     Gets or sets the name of the f.
        /// </summary>
        /// <value>
        ///     The name of the f.
        /// </value>
        public string FName { get; set; }

        /// <summary>
        ///     Gets or sets the name of the l.
        /// </summary>
        /// <value>
        ///     The name of the l.
        /// </value>
        public string LName { get; set; }

        /// <summary>
        ///     Gets or sets the address.
        /// </summary>
        /// <value>
        ///     The address.
        /// </value>
        public string Address { get; set; }

        /// <summary>
        ///     Gets or sets the dob.
        /// </summary>
        /// <value>
        ///     The dob.
        /// </value>
        public string Dob { get; set; }

        /// <summary>
        ///     Gets or sets the contact phone number.
        /// </summary>
        /// <value>
        ///     The contact phone number.
        /// </value>
        public string ContactPhoneNumber { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Patient" /> class.
        /// </summary>
        /// <param name="patientId">The patient identifier.</param>
        /// <param name="fName">Name of the f.</param>
        /// <param name="lName">Name of the l.</param>
        /// <param name="address">The address.</param>
        /// <param name="dob">The dob.</param>
        /// <param name="contactPhoneNUmber">The contact phone n umber.</param>
        public Patient(int patientId, string fName, string lName, Address address, string dob, string contactPhoneNUmber)
        {
            this.PatientId = patientId;
            this.FName = fName;
            this.LName = lName;
            this.theAddress = address;
            this.Address = "" + address.StreetAddress1 + " " + address.StreetAddress2 + " " + address.City + " " +
                           address.State + " " + address.Zip;
            this.Dob = dob;
            this.ContactPhoneNumber = contactPhoneNUmber;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Patient" /> class.
        /// </summary>
        /// <param name="fName">Name of the f.</param>
        /// <param name="lName">Name of the l.</param>
        /// <param name="address">The address.</param>
        /// <param name="dob">The dob.</param>
        /// <param name="contactPhoneNUmber">The contact phone n umber.</param>
        public Patient(string fName, string lName, Address address, string dob, string contactPhoneNUmber)
        {
            this.FName = fName;
            this.LName = lName;
            this.theAddress = address;
            this.Address = "" + address.StreetAddress1 + " " + address.StreetAddress2 + " " + address.City + " " +
                           address.State + " " + address.Zip;
            this.Dob = dob;
            this.ContactPhoneNumber = contactPhoneNUmber;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets the address object for patient.
        /// </summary>
        /// <returns></returns>
        public Address GetTheAddressObjectForPatient()
        {
            return this.theAddress;
        }

        #endregion
    }
}