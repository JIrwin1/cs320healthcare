﻿namespace HealthCareApplication.Model
{
    /// <summary>
    ///     Creates doctor class
    /// </summary>
    public class Doctor
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        ///     Gets or sets the fname.
        /// </summary>
        /// <value>
        ///     The fname.
        /// </value>
        public string Fname { get; set; }

        /// <summary>
        ///     Gets or sets the lname.
        /// </summary>
        /// <value>
        ///     The lname.
        /// </value>
        public string Lname { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Doctor" /> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="fname">The fname.</param>
        /// <param name="lname">The lname.</param>
        public Doctor(int id, string fname, string lname)
        {
            this.Id = id;
            this.Fname = fname;
            this.Lname = lname;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Doctor" /> class.
        /// </summary>
        /// <param name="fname">The fname.</param>
        /// <param name="lname">The lname.</param>
        public Doctor(string fname, string lname)
        {
            this.Fname = fname;
            this.Lname = lname;
        }

        #endregion
    }
}