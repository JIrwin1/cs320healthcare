﻿namespace HealthCareApplication.Model
{
    /// <summary>
    /// rot encryption for passwords
    /// </summary>
    public class RotEncription
    {
        #region Data members

        private const int BigA = 65;
        private const int BigZ = 90;
        private const int LittleA = 97;
        private const int LittleZ = 122;

        private readonly int numberOfEncriptionRotations;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="RotEncription"/> class.
        /// </summary>
        /// <param name="numberOfEncriptionRotations">The number of encription rotations.</param>
        public RotEncription(int numberOfEncriptionRotations)
        {
            this.numberOfEncriptionRotations = numberOfEncriptionRotations;
        }

        #endregion

        /// <summary>
        ///     Encripts the text using a rot13 like encription but uses a number given by the user to rotate with.
        /// </summary>
        /// <param name="textToEncrpit">The text to encrpit.</param>
        /// <returns>The encripted text</returns>
        public string EncriptText(string textToEncrpit)
        {
            if (string.IsNullOrEmpty(textToEncrpit))
            {
                return textToEncrpit;
            }

            var theArrayOfCharsMadeFromTheTextPassedIn = new char[textToEncrpit.Length];

            for (var currentIndexInArray = 0; currentIndexInArray < textToEncrpit.Length; currentIndexInArray++)
            {
                var theCharactorFromInputBeingEncripted = textToEncrpit[currentIndexInArray];
                if (theCharactorFromInputBeingEncripted >= LittleA && theCharactorFromInputBeingEncripted <= LittleZ)
                {
                    int theNewLetterThatHasBeenEncripted = theCharactorFromInputBeingEncripted;
                    for (var numberOfRotations = 0;
                        numberOfRotations < this.numberOfEncriptionRotations;
                        numberOfRotations++)
                    {
                        theNewLetterThatHasBeenEncripted = theCharactorFromInputBeingEncripted + 1;

                        if (theNewLetterThatHasBeenEncripted > LittleZ)
                        {
                            theNewLetterThatHasBeenEncripted -= 26;
                        }
                    }

                    theArrayOfCharsMadeFromTheTextPassedIn[currentIndexInArray] =
                        (char)theNewLetterThatHasBeenEncripted;
                }
                else if (theCharactorFromInputBeingEncripted >= BigA && theCharactorFromInputBeingEncripted <= BigZ)
                {
                    int theNewLetterThatHasBeenEncripted = theCharactorFromInputBeingEncripted;
                    for (var numberOfRotations = 0;
                        numberOfRotations < this.numberOfEncriptionRotations;
                        numberOfRotations++)
                    {
                        theNewLetterThatHasBeenEncripted = theCharactorFromInputBeingEncripted + 1;

                        if (theNewLetterThatHasBeenEncripted > BigZ)
                        {
                            theNewLetterThatHasBeenEncripted -= 26;
                        }
                    }

                    theArrayOfCharsMadeFromTheTextPassedIn[currentIndexInArray] =
                        (char)theNewLetterThatHasBeenEncripted;
                }
                else
                {
                    theArrayOfCharsMadeFromTheTextPassedIn[currentIndexInArray] = theCharactorFromInputBeingEncripted;
                }
            }
            return new string(theArrayOfCharsMadeFromTheTextPassedIn);
        }

       
    }
}
