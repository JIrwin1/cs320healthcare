﻿namespace HealthCareApplication.Model
{
    /// <summary>
    ///     Creates Administrator Class
    /// </summary>
    public class Admin
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the admin identifier.
        /// </summary>
        /// <value>
        ///     The admin identifier.
        /// </value>
        public int AdminId { get; set; }

        /// <summary>
        ///     Gets or sets the name of the f.
        /// </summary>
        /// <value>
        ///     The name of the f.
        /// </value>
        public string FName { get; set; }

        /// <summary>
        ///     Gets or sets the name of the l.
        /// </summary>
        /// <value>
        ///     The name of the l.
        /// </value>
        public string LName { get; set; }

        /// <summary>
        ///     Gets or sets the login identifier.
        /// </summary>
        /// <value>
        ///     The login identifier.
        /// </value>
        public int LoginId { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Admin" /> class.
        /// </summary>
        /// <param name="adminId">The admin identifier.</param>
        /// <param name="fName">Name of the f.</param>
        /// <param name="lName">Name of the l.</param>
        /// <param name="loginId">The login identifier.</param>
        public Admin(int adminId, string fName, string lName, int loginId)
        {
            this.AdminId = adminId;
            this.FName = fName;
            this.LName = lName;
            this.LoginId = loginId;
        }

        #endregion
    }
}