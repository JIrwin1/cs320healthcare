﻿namespace HealthCareApplication.Model
{
    /// <summary>
    ///     Creates the address for a patient
    /// </summary>
    public class Address
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the address identifier.
        /// </summary>
        /// <value>
        ///     The address identifier.
        /// </value>
        public int AddressId { get; set; }

        /// <summary>
        ///     Gets or sets the street address1.
        /// </summary>
        /// <value>
        ///     The street address1.
        /// </value>
        public string StreetAddress1 { get; set; }

        /// <summary>
        ///     Gets or sets the street address2.
        /// </summary>
        /// <value>
        ///     The street address2.
        /// </value>
        public string StreetAddress2 { get; set; }

        /// <summary>
        ///     Gets or sets the city.
        /// </summary>
        /// <value>
        ///     The city.
        /// </value>
        public string City { get; set; }

        /// <summary>
        ///     Gets or sets the state.
        /// </summary>
        /// <value>
        ///     The state.
        /// </value>
        public string State { get; set; }

        /// <summary>
        ///     Gets or sets the zip.
        /// </summary>
        /// <value>
        ///     The zip.
        /// </value>
        public string Zip { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Address" /> class.
        /// </summary>
        /// <param name="addressId">The address identifier.</param>
        /// <param name="streetAddress1">The street address1.</param>
        /// <param name="streetAddress2">The street address2.</param>
        /// <param name="city">The city.</param>
        /// <param name="state">The state.</param>
        /// <param name="zip">The zip.</param>
        public Address(int addressId, string streetAddress1, string streetAddress2, string city, string state,
            string zip)
        {
            this.AddressId = addressId;
            this.StreetAddress1 = streetAddress1;
            this.StreetAddress2 = streetAddress2;
            this.City = city;
            this.State = state;
            this.Zip = zip;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Address" /> class.
        /// </summary>
        /// <param name="streetAddress1">The street address1.</param>
        /// <param name="streetAddress2">The street address2.</param>
        /// <param name="city">The city.</param>
        /// <param name="state">The state.</param>
        /// <param name="zip">The zip.</param>
        public Address(string streetAddress1, string streetAddress2, string city, string state, string zip)
        {
            this.StreetAddress1 = streetAddress1;
            this.StreetAddress2 = streetAddress2;
            this.City = city;
            this.State = state;
            this.Zip = zip;
        }

        #endregion
    }
}