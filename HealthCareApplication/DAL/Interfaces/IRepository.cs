﻿using System.Collections.Generic;

namespace HealthCareApplication.DAL.Interfaces
{
    internal interface IRepository<T>
    {
        #region Methods

        void Add(T entity);

        T GetByld(int id);

        IList<T> GetAll();

        #endregion
    }
}