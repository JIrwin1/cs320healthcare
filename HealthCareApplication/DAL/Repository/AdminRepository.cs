﻿using System;
using System.Collections.Generic;
using System.Data;
using HealthCareApplication.DAL.Interfaces;
using HealthCareApplication.Model;
using HealthCareApplication.View;
using MySql.Data.MySqlClient;

namespace HealthCareApplication.DAL.Repository
{
    internal class AdminRepository : IRepository<Admin>
    {
        #region Data members

        private readonly MySqlConnection connection;
        private Admin targetAdmin;

        #endregion

        #region Constructors

        public AdminRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connection = DbConnection.GetConnection(connectionLabel);
        }

        #endregion

        #region Methods

        public void Add(Admin theAdminBeingAdded)
        {
            {
                const string sql =
                    " INSERT INTO ADMIN(fName, lName, userID_loginID) " +
                    " VALUES (@fName, @lName, @userID_loginID)";

                using (var cmd = new MySqlCommand(sql))

                {
                    cmd.Parameters.AddWithValue("@fName", theAdminBeingAdded.FName);
                    cmd.Parameters.AddWithValue("@lName", theAdminBeingAdded.LName);
                    cmd.Parameters.AddWithValue("@userID_loginID", theAdminBeingAdded.LoginId);

                    cmd.Connection = this.connection;
                    cmd.Connection.Open();

                    cmd.ExecuteNonQuery();

                    cmd.Connection.Close();
                }
            }
        }

        public IList<Admin> GetAll()
        {
            var listOfAdmins = new List<Admin>();

            const string sql = "SELECT * From ADMIN";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        listOfAdmins.Add(new Admin(Convert.ToInt32(reader["adminID"]),
                            Convert.ToString(reader["fName"]), Convert.ToString(reader["lName"]),
                            Convert.ToInt32(reader["userID_loginID"])));
                    }
                }

                cmd.Connection.Close();
            }
            return listOfAdmins;
        }

        public Admin GetByld(int id)
        {
            const string sql = "SELECT * From ADMIN WHERE " + "adminID = @adminID";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@adminID", id);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        this.targetAdmin = new Admin(Convert.ToInt32(reader["adminID"]),
                            Convert.ToString(reader["fName"]), Convert.ToString(reader["lName"]),
                            Convert.ToInt32(reader["userID_loginID"]));
                    }
                }

                cmd.Connection.Close();
            }
            return this.targetAdmin;
        }

        internal void ImputSqlCommandForAdmin(string theSqlCommand)
        {
            var sql = theSqlCommand;

            using (var cmd = new MySqlCommand(sql))
            {
                cmd.Connection = this.connection;

                cmd.Connection.Open();

                var dt = new DataTable();
                using (var sqlDataReader = cmd.ExecuteReader())
                {

                    {
                        dt.Load(sqlDataReader);
                        sqlDataReader.Close();
                        AdminForm.Data = dt;
                        sqlDataReader.Close();

                        cmd.Connection.Close();
                    }
                }

                cmd.Connection.Close();
            }
        }

        public Admin GetAdminByLoginId(string loginId)
        {
            const string sql = "SELECT * From ADMIN WHERE " + "userID_loginID = @loginID";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@loginID", loginId);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        this.targetAdmin = new Admin(Convert.ToInt32(reader["adminID"]),
                            Convert.ToString(reader["fName"]), Convert.ToString(reader["lName"]),
                            Convert.ToInt32(reader["userID_loginID"]));
                    }
                }

                cmd.Connection.Close();
            }
            return this.targetAdmin;
        }

        public bool CheckToSeeIfCorrectPasswordIsBeingUsed(string passwordToBeChecked,
            Admin adminThatWeNeedToCheckPasswordOf)
        {
            var loggedInCOrrectly = false;
            const string sql =
                "SELECT * From ADMIN, USERID, CREDENTIALS WHERE " +
                "adminID = @adminID AND ADMIN.userID_loginID = loginID AND loginID = CREDENTIALS.userID_loginID AND CREDENTIALS.password = @password";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@adminID", adminThatWeNeedToCheckPasswordOf.AdminId);
                cmd.Parameters.AddWithValue("@password", passwordToBeChecked);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        loggedInCOrrectly = true;
                    }
                }

                cmd.Connection.Close();
            }
            return loggedInCOrrectly;
        }

        #endregion
    }
}