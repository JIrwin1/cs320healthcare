﻿using System;
using System.Collections.Generic;
using HealthCareApplication.DAL.Interfaces;
using HealthCareApplication.Model;
using MySql.Data.MySqlClient;

namespace HealthCareApplication.DAL.Repository
{
    /// <summary>
    ///     Creates Repository for Routine Checks
    /// </summary>
    /// <seealso cref="RoutineChecks" />
    public class RoutineChecksRepository : IRepository<RoutineChecks>
    {
        #region Data members

        private readonly MySqlConnection connection;
        private RoutineChecks routineChecks;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="RoutineChecksRepository" /> class.
        /// </summary>
        /// <param name="connectionLabel">The connection label.</param>
        public RoutineChecksRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connection = DbConnection.GetConnection(connectionLabel);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Adds the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Add(RoutineChecks entity)
        {
            {
                const string sql =
                    " INSERT INTO `ROUTINE_CHECKS` (`Body_temperature`, `Blood_Pressure`, `Pulse`, `Symptoms`, `Nurses_NurseId`) " +
                    " VALUES (@temp, @bp, @pulse, @symptoms, @nurseId)";

                using (var cmd = new MySqlCommand(sql))

                {
                    cmd.Parameters.AddWithValue("@temp", entity.BodyTemprature);
                    cmd.Parameters.AddWithValue("@bp", entity.BloodPresure);
                    cmd.Parameters.AddWithValue("@pulse", entity.Pulse);
                    cmd.Parameters.AddWithValue("@symptoms", entity.Symptoms);
                    cmd.Parameters.AddWithValue("@nurseId", entity.NurseId);

                    cmd.Connection = this.connection;
                    cmd.Connection.Open();

                    cmd.ExecuteNonQuery();

                    cmd.Connection.Close();
                }
            }
        }

        /// <summary>
        ///     Gets the byld.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Routine Checks by Id</returns>
        public RoutineChecks GetByld(int id)
        {
            const string sql = "SELECT * From ROUTINE_CHECKS WHERE " + "Routine_check_id = @checkId";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@checkId", id);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        this.routineChecks = new RoutineChecks(Convert.ToInt32(reader["Routine_check_id"]),
                            Convert.ToDecimal(reader["Body_temperature"]),
                            Convert.ToString(reader["Blood_Pressure"]),
                            Convert.ToString(reader["Pulse"]),
                            Convert.ToString(reader["Symptoms"]),
                            Convert.ToInt32(reader["Nurses_NurseId"]));
                    }
                }

                cmd.Connection.Close();
            }
            return this.routineChecks;
        }

        /// <summary>
        ///     Gets all.
        /// </summary>
        /// <returns>A list of all Routine Checks</returns>
        public IList<RoutineChecks> GetAll()
        {
            var listOfRoutineChecks = new List<RoutineChecks>();

            const string sql = "SELECT * From ROUTINE_CHECKS";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        listOfRoutineChecks.Add(new RoutineChecks(Convert.ToInt32(reader["Routine_check_id"]),
                            Convert.ToDecimal(reader["Body_temperature"]),
                            Convert.ToString(reader["Blood_Pressure"]),
                            Convert.ToString(reader["Pulse"]),
                            Convert.ToString(reader["Symptoms"]),
                            Convert.ToInt32(reader["Nurses_NurseId"])));
                    }
                }

                cmd.Connection.Close();
            }
            return listOfRoutineChecks;
        }

        /// <summary>
        ///     Gets the last made check identifier.
        /// </summary>
        /// <returns>Last made Routine Check Id</returns>
        public int GetLastMadeCheckId()
        {
            var returnId = 0;

            const string sql =
                "SELECT MAX(Routine_check_id) AS maxid FROM ROUTINE_CHECKS;";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        returnId = Convert.ToInt32(reader["maxid"]);
                    }
                }

                cmd.Connection.Close();
            }

            return returnId;
        }

        /// <summary>
        ///     Gets the routine checks of patient.
        /// </summary>
        /// <param name="thePatient">The patient.</param>
        /// <returns>Routine Checks of a Patient</returns>
        public List<RoutineChecks> GetRoutineChecksOfPatient(Patient thePatient)
        {
            const string sql = "SELECT * " +
                               " From ROUTINE_CHECKS, PATIENT, APPOINTMENTS " +
                               " WHERE PatientID = @patId AND Patient_Patientid = PatientID AND Routine_checks_Routine_check_id = Routine_check_id ";
            var checks = new List<RoutineChecks>();
            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@patid", thePatient.PatientId);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        checks.Add(new RoutineChecks(Convert.ToInt32(reader["Routine_check_id"]),
                            Convert.ToDecimal(reader["Body_temperature"]),
                            Convert.ToString(reader["Blood_Pressure"]),
                            Convert.ToString(reader["Pulse"]),
                            Convert.ToString(reader["Symptoms"]),
                            Convert.ToInt32(reader["Nurses_NurseId"])));
                    }
                }

                cmd.Connection.Close();
            }
            return checks;
        }

        /// <summary>
        ///     Checks to see if check exists.
        /// </summary>
        /// <param name="appointmentId">The appointment identifier.</param>
        /// <returns>If a patient exists</returns>
        public bool CheckToSeeIfCheckExists(int appointmentId)
        {
            const string sql =
                "SELECT * From ROUTINE_CHECKS, APPOINTMENTS WHERE " +
                " Routine_checks_Routine_check_id = Routine_check_id AND AppointmentID = @aid ";

            var doctorExist = false;

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@aid", appointmentId);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        doctorExist = true;
                    }
                }

                cmd.Connection.Close();
            }
            return doctorExist;
        }

        #endregion
    }
}