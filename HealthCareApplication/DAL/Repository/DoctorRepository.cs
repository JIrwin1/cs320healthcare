﻿using System;
using System.Collections.Generic;
using HealthCareApplication.DAL.Interfaces;
using HealthCareApplication.Model;
using MySql.Data.MySqlClient;

namespace HealthCareApplication.DAL.Repository
{
    internal class DoctorRepository : IRepository<Doctor>
    {
        #region Data members

        private readonly MySqlConnection connection;
        private Doctor targetDoctor;

        #endregion

        #region Constructors

        public DoctorRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connection = DbConnection.GetConnection(connectionLabel);
        }

        #endregion

        #region Methods

        public void Add(Doctor theDoctorBeingAdded)
        {
            {
                const string sql =
                    " INSERT INTO DOCTORS(fName, lName) " +
                    " VALUES (@fName, @lName)";

                using (var cmd = new MySqlCommand(sql))

                {
                    cmd.Parameters.AddWithValue("@fName", theDoctorBeingAdded.Fname);
                    cmd.Parameters.AddWithValue("@lName", theDoctorBeingAdded.Lname);

                    cmd.Connection = this.connection;
                    cmd.Connection.Open();

                    cmd.ExecuteNonQuery();

                    cmd.Connection.Close();
                }
            }
        }

        public IList<Doctor> GetAll()
        {
            var listOfDoctors = new List<Doctor>();

            const string sql = "SELECT * From DOCTORS";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        listOfDoctors.Add(new Doctor(Convert.ToInt32(reader["DoctorID"]),
                            Convert.ToString(reader["fName"]), Convert.ToString(reader["lName"])));
                    }
                }

                cmd.Connection.Close();
            }
            return listOfDoctors;
        }

        public Doctor GetByld(int id)
        {
            const string sql = "SELECT * From DOCTORS WHERE " + "DoctorID = @doctorID";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@doctorID", id);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        this.targetDoctor = new Doctor(Convert.ToInt32(reader["DoctorID"]),
                            Convert.ToString(reader["fName"]), Convert.ToString(reader["lName"]));
                    }
                }

                cmd.Connection.Close();
            }
            return this.targetDoctor;
        }

        public bool CheckToSeeIfDocotorExists(int doctorIdWeWantToCHeckIfExists)
        {
            const string sql =
                "SELECT * From DOCTORS WHERE " +
                "DoctorID = @id";

            var doctorExist = false;

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@id", doctorIdWeWantToCHeckIfExists);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        doctorExist = true;
                    }
                }

                cmd.Connection.Close();
            }
            return doctorExist;
        }

        #endregion
    }
}