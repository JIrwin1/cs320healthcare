﻿using System;
using System.Collections.Generic;
using HealthCareApplication.DAL.Interfaces;
using HealthCareApplication.Model;
using MySql.Data.MySqlClient;

namespace HealthCareApplication.DAL.Repository
{
    internal class NurseRepository : IRepository<Nurse>
    {
        #region Data members

        private readonly MySqlConnection connection;

        private Nurse targetNurse;

        #endregion

        #region Constructors

        public NurseRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connection = DbConnection.GetConnection(connectionLabel);
        }

        #endregion

        #region Methods

        public void Add(Nurse theNurseBeingAdded)
        {
            const string sql =
                " INSERT INTO NURSES(fName, lName, userID_loginID) " +
                " VALUES (@fName, @lName, @userID_loginID)";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@fName", theNurseBeingAdded.FName);
                cmd.Parameters.AddWithValue("@lName", theNurseBeingAdded.LName);
                cmd.Parameters.AddWithValue("@userID_loginID", theNurseBeingAdded.LoginId);

                cmd.Connection = this.connection;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();

                cmd.Connection.Close();
            }
        }

        public IList<Nurse> GetAll()

        {
            var listOfNurses = new List<Nurse>();

            const string sql = "SELECT * From NURSES";

            using (var cmd = new MySqlCommand(sql))
            {
                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        listOfNurses.Add(new Nurse(Convert.ToInt32(reader["NurseId"]),
                            Convert.ToString(reader["fName"]), Convert.ToString(reader["lName"]),
                            Convert.ToInt32(reader["userID_loginID"])));
                    }
                }

                cmd.Connection.Close();
            }
            return listOfNurses;
        }

        public Nurse GetByld(int id)
        {
            const string sql = "SELECT * From NURSES WHERE " + "NurseId = @nurseLoginId";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@nurseLoginId", id);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        this.targetNurse = new Nurse(Convert.ToInt32(reader["NurseId"]),
                            Convert.ToString(reader["fName"]), Convert.ToString(reader["lName"]),
                            Convert.ToInt32(reader["userID_loginID"]));
                    }
                }

                cmd.Connection.Close();
            }
            return this.targetNurse;
        }

        public Nurse GetNurseByLoginId(string nurseLoginId)
        {
            {
                const string sql = "SELECT * From NURSES WHERE " + "userID_loginID = @loginID";

                using (var cmd = new MySqlCommand(sql))

                {
                    cmd.Parameters.AddWithValue("@loginID", nurseLoginId);

                    cmd.Connection = this.connection;
                    cmd.Connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())

                        {
                            this.targetNurse = new Nurse(Convert.ToInt32(reader["NurseId"]),
                                Convert.ToString(reader["fName"]), Convert.ToString(reader["lName"]),
                                Convert.ToInt32(reader["userID_loginID"]));
                        }
                    }

                    cmd.Connection.Close();
                }
                return this.targetNurse;
            }
        }

        public bool CheckToSeeIfCorrectPasswordIsBeingUsed(string passwordToBeChecked,
            Nurse nurseThatWeAreCheckingPasswordFor)
        {
            const string sql =
                "SELECT * From NURSES, USERID, CREDENTIALS  WHERE " +
                "NurseId = @NurseID AND NURSES.userID_loginID = loginID AND loginID = CREDENTIALS.userID_loginID AND CREDENTIALS.password = @password";
            var correctlyLoggenedIn = false;
            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@NurseID", nurseThatWeAreCheckingPasswordFor.NurseId);
                cmd.Parameters.AddWithValue("@password", passwordToBeChecked);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        correctlyLoggenedIn = true;
                    }
                }

                cmd.Connection.Close();
            }

            return correctlyLoggenedIn;
        }

        #endregion
    }
}