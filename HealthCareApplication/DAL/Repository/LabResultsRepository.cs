﻿using System;
using System.Collections.Generic;
using HealthCareApplication.DAL.Interfaces;
using HealthCareApplication.Model;
using MySql.Data.MySqlClient;

namespace HealthCareApplication.DAL.Repository
{
    internal class LabResultsRepository : IRepository<LabResults>
    {
        #region Data members

        private readonly MySqlConnection connection;

        #endregion

        #region Constructors

        public LabResultsRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connection = DbConnection.GetConnection(connectionLabel);
        }

        #endregion

        #region Methods

        public void Add(LabResults entity)
        {
            const string sql =
                " INSERT INTO `LAB_RESULTS` (`TestType`, `results`, `performDate`) " +
                " VALUES (@type, @result, @date)";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@type", entity.GetTestId());
                cmd.Parameters.AddWithValue("@result", entity.Results);
                cmd.Parameters.AddWithValue("@date", entity.PerformOnDate);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                cmd.ExecuteNonQuery();

                cmd.Connection.Close();
            }
        }

        public IList<LabResults> GetAll()
        {
            throw new NotImplementedException();
        }

        public LabResults GetByld(int id)
        {
            throw new NotImplementedException();
        }

        public Dictionary<int, string> GetTestTypes()
        {
            var testTypes = new Dictionary<int, string>();
            const string sql = "SELECT * " +
                               " From  TESTS ";
          
            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        testTypes.Add(Convert.ToInt32(reader["TestId"]), Convert.ToString(reader["TestName"]));
                    }
                }

                cmd.Connection.Close();
            }
            return testTypes;
        }

        internal void UpdateResultOfTest(int testId, string testResults)
        {
            const string sql =
                "UPDATE LAB_RESULTS "
                + "SET results = @results "
                + "WHERE TestID = @TestID ";

            using (var cmd = new MySqlCommand(sql))
            {
                cmd.Parameters.AddWithValue("@results", testResults);
                cmd.Parameters.AddWithValue("@TestID", testId);
                cmd.Connection = this.connection;

                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        cmd.Connection.Open();

                        cmd.ExecuteNonQuery();

                        cmd.Connection.Close();
                    }
                }

                cmd.Connection.Close();
            }
        }

        public void AddToInBetweenTableForResultAndAppointment(int appointmentid, int labResultId)
        {
            const string sql =
                " INSERT INTO `APPOINTMENT_HAS_TEST_RESULTS` (`Lab_results_TestID`, `Appointments_AppointmentID`) " +
                " VALUES (@Lab_results_TestID, @Appointments_AppointmentID)";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@Lab_results_TestID", labResultId);
                cmd.Parameters.AddWithValue("@Appointments_AppointmentID", appointmentid);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                cmd.ExecuteNonQuery();

                cmd.Connection.Close();
            }
        }

        public int GetLastMadeCheckId()
        {
            var returnId = 0;

            const string sql =
                "SELECT MAX(TestID) AS lastId FROM LAB_RESULTS;";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        returnId = Convert.ToInt32(reader["lastId"]);
                    }
                }

                cmd.Connection.Close();
            }

            return returnId;
        }

        public List<LabResults> GetPatientTests(Patient thePatient)
        {
            const string sql = "SELECT * " +
                               " From  PATIENT, APPOINTMENTS, LAB_RESULTS, APPOINTMENT_HAS_TEST_RESULTS, TESTS " +
                               " WHERE PatientID = @patId AND Patient_Patientid = PatientID AND Appointments_AppointmentID = AppointmentID AND LAB_RESULTS.TestID = Lab_results_TestID AND TestType = TESTS.TestId";
            var tests = new List<LabResults>();
            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@patid", thePatient.PatientId);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        tests.Add(new LabResults(Convert.ToInt32(reader["TestID"]),
                            Convert.ToString(reader["TestName"]),
                            Convert.ToString(reader["results"]),
                            Convert.ToDateTime(reader["performDate"])));
                    }
                }

                cmd.Connection.Close();
            }
            return tests;
        }

        public Boolean CheckToSeeIfLabResultHasANotResults(int labid)
        {
            const string sql =
                "SELECT * " +
                " From LAB_RESULTS " +
                " WHERE TestID = @labid AND results = '' ";


            var resultExist = false;

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@labid", labid);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        resultExist = true;
                    }
                }

                cmd.Connection.Close();
            }
            return resultExist;
        }

        #endregion
    }
}