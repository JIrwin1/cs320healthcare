﻿using System;
using System.Collections.Generic;
using HealthCareApplication.DAL.Interfaces;
using HealthCareApplication.Model;
using MySql.Data.MySqlClient;

namespace HealthCareApplication.DAL.Repository
{
    internal class PatientReposiotry : IRepository<Patient>
    {
        #region Data members

        private readonly MySqlConnection connection;

        private Patient targetPatient;

        #endregion

        #region Constructors

        public PatientReposiotry(string connectionLabel = "MySqlDbConnection")
        {
            this.connection = DbConnection.GetConnection(connectionLabel);
        }

        #endregion

        #region Methods

        public void Add(Patient thePatientBeingAdded)
        {
            this.createAddressForPatientBeingAdded(thePatientBeingAdded);
            this.setPatientAddressByGettingMaxId(thePatientBeingAdded);

            const string sql =
                " INSERT INTO PATIENT(`fName`, `lName`, `ADDRESS_ADDRESSID`, `DOB`, `Contact_phone_number`) " +
                " VALUES (@fName, @lName, @address, @dateOfBirth, @contactPhoneNumber)";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@fName", thePatientBeingAdded.FName);
                cmd.Parameters.AddWithValue("@lName", thePatientBeingAdded.LName);
                cmd.Parameters.AddWithValue("@address", thePatientBeingAdded.GetTheAddressObjectForPatient().AddressId);
                cmd.Parameters.AddWithValue("@dateOfBirth", Convert.ToDateTime(thePatientBeingAdded.Dob));
                cmd.Parameters.AddWithValue("@contactPhoneNumber", thePatientBeingAdded.ContactPhoneNumber);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                cmd.ExecuteNonQuery();

                cmd.Connection.Close();
            }
        }

        public IList<Patient> GetAll()
        {
            var listOfPatients = new List<Patient>();

            const string sql = "SELECT * " +
                               " FROM PATIENT, ADDRESS " +
                               " WHERE PATIENT.ADDRESS_ADDRESSID = ADDRESS.addressID";

            using (var cmd = new MySqlCommand(sql))
            {
                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        listOfPatients.Add(new Patient(Convert.ToInt32(reader["PatientId"]),
                            Convert.ToString(reader["fName"]), Convert.ToString(reader["lName"]),
                            new Address(Convert.ToInt32(reader["addressID"]),
                                Convert.ToString(reader["streetAddress1"]),
                                Convert.ToString(reader["streetAddress2"]), Convert.ToString(reader["city"]),
                                Convert.ToString(reader["state"]), Convert.ToString(reader["zip"])),
                            Convert.ToString(reader["DOB"]),
                            Convert.ToString(reader["Contact_Phone_number"])));
                    }
                }

                cmd.Connection.Close();
            }
            return listOfPatients;
        }

        public Patient GetByld(int id)
        {
            const string sql = "SELECT * " +
                               " FROM PATIENT, ADDRESS " +
                               " WHERE PatientId = @PatientId AND PATIENT.ADDRESS_ADDRESSID = ADDRESS.addressID";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@PatientId", id);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        this.targetPatient = new Patient(Convert.ToInt32(reader["PatientId"]),
                            Convert.ToString(reader["fName"]), Convert.ToString(reader["lName"]),
                            new Address(Convert.ToInt32(reader["addressID"]),
                                Convert.ToString(reader["streetAddress1"]),
                                Convert.ToString(reader["streetAddress2"]), Convert.ToString(reader["city"]),
                                Convert.ToString(reader["state"]), Convert.ToString(reader["zip"])),
                            Convert.ToString(reader["DOB"]),
                            Convert.ToString(reader["Contact_phone_number"]));
                    }
                }

                cmd.Connection.Close();
            }
            return this.targetPatient;
        }

        private void setPatientAddressByGettingMaxId(Patient thePatientBeingAdded)
        {
            const string sql =
                "SELECT MAX(addressID) AS HighestStreetAddress FROM ADDRESS;";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        thePatientBeingAdded.GetTheAddressObjectForPatient().AddressId =
                            Convert.ToInt32(reader["HighestStreetAddress"]);
                    }
                }

                cmd.Connection.Close();
            }
        }

        private void createAddressForPatientBeingAdded(Patient thePatientBeingAdded)
        {
            const string sql =
                " INSERT INTO `ADDRESS` (`streetAddress1`, `streetAddress2`, `city`, `state`, `zip`) " +
                " VALUES (@streetAddress1, @streetAddress2, @city, @state, @zip)";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@streetAddress1",
                       Convert.ToString(thePatientBeingAdded.GetTheAddressObjectForPatient().StreetAddress1));
                cmd.Parameters.AddWithValue("@streetAddress2",
                       Convert.ToString(thePatientBeingAdded.GetTheAddressObjectForPatient().StreetAddress2));
                cmd.Parameters.AddWithValue("@city",
                       Convert.ToString(thePatientBeingAdded.GetTheAddressObjectForPatient().City));
                cmd.Parameters.AddWithValue("@state",
                       Convert.ToString(thePatientBeingAdded.GetTheAddressObjectForPatient().State));
                cmd.Parameters.AddWithValue("@zip",
                       Convert.ToString(thePatientBeingAdded.GetTheAddressObjectForPatient().Zip));

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                cmd.ExecuteNonQuery();

                cmd.Connection.Close();
            }
        }

        public List<Patient> GetPatientsFromGivenInformationbyFirstName(string firstName)
        {
            {
                var listOfPatients = new List<Patient>();

                const string sql =
                    "SELECT * " +
                    " FROM PATIENT, ADDRESS " +
                    " WHERE PATIENT.fName = @fname AND PATIENT.ADDRESS_ADDRESSID = ADDRESS.addressID";

                using (var cmd = new MySqlCommand(sql))

                {
                    cmd.Parameters.AddWithValue("@fname", firstName);

                    cmd.Connection = this.connection;
                    cmd.Connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())

                        {
                            listOfPatients.Add(new Patient(Convert.ToInt32(reader["PatientId"]),
                                Convert.ToString(reader["fName"]), Convert.ToString(reader["lName"]),
                                new Address(Convert.ToInt32(reader["addressID"]),
                                    Convert.ToString(reader["streetAddress1"]),
                                    Convert.ToString(reader["streetAddress2"]), Convert.ToString(reader["city"]),
                                    Convert.ToString(reader["state"]), Convert.ToString(reader["zip"])),
                                Convert.ToString(reader["DOB"]),
                                Convert.ToString(reader["Contact_phone_number"])));
                        }
                    }

                    cmd.Connection.Close();
                }
                return listOfPatients;
            }
        }

        public List<Patient> GetPatientsFromGivenInformationByLastName(string lastName)
        {
            {
                var listOfPatients = new List<Patient>();

                const string sql =
                    "SELECT * " +
                    " FROM PATIENT, ADDRESS " +
                    " WHERE PATIENT.lName = @lname AND PATIENT.ADDRESS_ADDRESSID = ADDRESS.addressID";

                using (var cmd = new MySqlCommand(sql))

                {
                    cmd.Parameters.AddWithValue("@lname", lastName);

                    cmd.Connection = this.connection;
                    cmd.Connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())

                        {
                            listOfPatients.Add(new Patient(Convert.ToInt32(reader["PatientId"]),
                                Convert.ToString(reader["fName"]), Convert.ToString(reader["lName"]),
                                new Address(Convert.ToInt32(reader["addressID"]),
                                    Convert.ToString(reader["streetAddress1"]),
                                    Convert.ToString(reader["streetAddress2"]), Convert.ToString(reader["city"]),
                                    Convert.ToString(reader["state"]), Convert.ToString(reader["zip"])),
                                Convert.ToString(reader["DOB"]),
                                Convert.ToString(reader["Contact_phone_number"])));
                        }
                    }

                    cmd.Connection.Close();
                }
                return listOfPatients;
            }
        }

        public List<Patient> GetPatientsFromGivenInformationByDob(DateTime dob)
        {
            {
                var listOfPatients = new List<Patient>();

                const string sql =
                    "SELECT * " +
                    " FROM PATIENT, ADDRESS " +
                    " WHERE PATIENT.DOB = @dob AND PATIENT.ADDRESS_ADDRESSID = ADDRESS.addressID";

                using (var cmd = new MySqlCommand(sql))

                {
                    cmd.Parameters.AddWithValue("@dob", dob);

                    cmd.Connection = this.connection;
                    cmd.Connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())

                        {
                            listOfPatients.Add(new Patient(Convert.ToInt32(reader["PatientId"]),
                                Convert.ToString(reader["fName"]), Convert.ToString(reader["lName"]),
                                new Address(Convert.ToInt32(reader["addressID"]),
                                    Convert.ToString(reader["streetAddress1"]),
                                    Convert.ToString(reader["streetAddress2"]), Convert.ToString(reader["city"]),
                                    Convert.ToString(reader["state"]), Convert.ToString(reader["zip"])),
                                Convert.ToString(reader["DOB"]),
                                Convert.ToString(reader["Contact_phone_number"])));
                        }
                    }

                    cmd.Connection.Close();
                }
                return listOfPatients;
            }
        }

        public List<Patient> GetPatientsFromGivenInformationByFirstLastName(string firstName, string lastName)
        {
            {
                var listOfPatients = new List<Patient>();

                const string sql =
                    "SELECT * " +
                    " FROM PATIENT, ADDRESS " +
                    " WHERE PATIENT.fName = @fname AND PATIENT.lName = @lname AND PATIENT.ADDRESS_ADDRESSID = ADDRESS.addressID";

                using (var cmd = new MySqlCommand(sql))

                {
                    cmd.Parameters.AddWithValue("@fname", firstName);
                    cmd.Parameters.AddWithValue("@lname", lastName);

                    cmd.Connection = this.connection;
                    cmd.Connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())

                        {
                            listOfPatients.Add(new Patient(Convert.ToInt32(reader["PatientId"]),
                                Convert.ToString(reader["fName"]), Convert.ToString(reader["lName"]),
                                new Address(Convert.ToInt32(reader["addressID"]),
                                    Convert.ToString(reader["streetAddress1"]),
                                    Convert.ToString(reader["streetAddress2"]), Convert.ToString(reader["city"]),
                                    Convert.ToString(reader["state"]), Convert.ToString(reader["zip"])),
                                Convert.ToString(reader["DOB"]),
                                Convert.ToString(reader["Contact_phone_number"])));
                        }
                    }

                    cmd.Connection.Close();
                }
                return listOfPatients;
            }
        }

        public List<Patient> GetPatientsFromGivenInformationByFirstNameDob(string firstName, DateTime date)
        {
            {
                var listOfPatients = new List<Patient>();

                const string sql =
                    "SELECT * " +
                    " FROM PATIENT, ADDRESS " +
                    " WHERE PATIENT.fName = @fname AND PATIENT.DOB = @dob AND PATIENT.ADDRESS_ADDRESSID = ADDRESS.addressID";

                using (var cmd = new MySqlCommand(sql))

                {
                    cmd.Parameters.AddWithValue("@fname", firstName);
                    cmd.Parameters.AddWithValue("@dob", date);

                    cmd.Connection = this.connection;
                    cmd.Connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())

                        {
                            listOfPatients.Add(new Patient(Convert.ToInt32(reader["PatientId"]),
                                Convert.ToString(reader["fName"]), Convert.ToString(reader["lName"]),
                                new Address(Convert.ToInt32(reader["addressID"]),
                                    Convert.ToString(reader["streetAddress1"]),
                                    Convert.ToString(reader["streetAddress2"]), Convert.ToString(reader["city"]),
                                    Convert.ToString(reader["state"]), Convert.ToString(reader["zip"])),
                                Convert.ToString(reader["DOB"]),
                                Convert.ToString(reader["Contact_phone_number"])));
                        }
                    }

                    cmd.Connection.Close();
                }
                return listOfPatients;
            }
        }

        public List<Patient> GetPatientsFromGivenInformationByLastNameDob(string lastName, DateTime date)
        {
            {
                var listOfPatients = new List<Patient>();

                const string sql =
                    "SELECT * " +
                    " FROM PATIENT, ADDRESS " +
                    " WHERE PATIENT.lName = @lname AND PATIENT.DOB = @dob AND PATIENT.ADDRESS_ADDRESSID = ADDRESS.addressID";

                using (var cmd = new MySqlCommand(sql))

                {
                    cmd.Parameters.AddWithValue("@lname", lastName);
                    cmd.Parameters.AddWithValue("@dob", date);

                    cmd.Connection = this.connection;
                    cmd.Connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())

                        {
                            listOfPatients.Add(new Patient(Convert.ToInt32(reader["PatientId"]),
                                Convert.ToString(reader["fName"]), Convert.ToString(reader["lName"]),
                                new Address(Convert.ToInt32(reader["addressID"]),
                                    Convert.ToString(reader["streetAddress1"]),
                                    Convert.ToString(reader["streetAddress2"]), Convert.ToString(reader["city"]),
                                    Convert.ToString(reader["state"]), Convert.ToString(reader["zip"])),
                                Convert.ToString(reader["DOB"]),
                                Convert.ToString(reader["Contact_phone_number"])));
                        }
                    }

                    cmd.Connection.Close();
                }
                return listOfPatients;
            }
        }

        public List<Patient> GetPatientsFromGivenInformationByFirstLastNameDob(string firstname, string lastName,
            DateTime date)
        {
            {
                var listOfPatients = new List<Patient>();

                const string sql =
                    "SELECT * " +
                    " FROM PATIENT, ADDRESS " +
                    " WHERE PATIENT.fName = @fname AND PATIENT.DOB = @dob AND PATIENT.lName = @lname AND PATIENT.ADDRESS_ADDRESSID = ADDRESS.addressID";

                using (var cmd = new MySqlCommand(sql))

                {
                    cmd.Parameters.AddWithValue("@fname", firstname);
                    cmd.Parameters.AddWithValue("@lname", lastName);
                    cmd.Parameters.AddWithValue("@dob", date);

                    cmd.Connection = this.connection;
                    cmd.Connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())

                        {
                            listOfPatients.Add(new Patient(Convert.ToInt32(reader["PatientId"]),
                                Convert.ToString(reader["fName"]), Convert.ToString(reader["lName"]),
                                new Address(Convert.ToInt32(reader["addressID"]),
                                    Convert.ToString(reader["streetAddress1"]),
                                    Convert.ToString(reader["streetAddress2"]), Convert.ToString(reader["city"]),
                                    Convert.ToString(reader["state"]), Convert.ToString(reader["zip"])),
                                Convert.ToString(reader["DOB"]),
                                Convert.ToString(reader["Contact_phone_number"])));
                        }
                    }

                    cmd.Connection.Close();
                }
                return listOfPatients;
            }
        }

        public bool CheckIfPatientExists(int patientWantToCHeckIfExists)
        {
            const string sql =
                "SELECT * From PATIENT WHERE " +
                "PatientID= @id";
            var patExists = false;

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@id", patientWantToCHeckIfExists);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        patExists = true;
                    }
                }

                cmd.Connection.Close();
            }
            return patExists;
        }

        #endregion
    }
}