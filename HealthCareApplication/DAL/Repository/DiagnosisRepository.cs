﻿using System;
using System.Collections.Generic;
using HealthCareApplication.DAL.Interfaces;
using HealthCareApplication.Model;
using MySql.Data.MySqlClient;

namespace HealthCareApplication.DAL.Repository
{
    /// <summary>
    ///     Creates Diagnosis Repository
    /// </summary>
    /// <seealso cref="Diagnosis" />
    public class DiagnosisRepository : IRepository<Diagnosis>
    {
        #region Data members

        private readonly MySqlConnection connection;
        private Diagnosis diagnosis;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="DiagnosisRepository" /> class.
        /// </summary>
        /// <param name="connectionLabel">The connection label.</param>
        public DiagnosisRepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connection = DbConnection.GetConnection(connectionLabel);
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Adds the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public void Add(Diagnosis entity)
        {
            {
                const string sql =
                    " INSERT INTO `DIAGNOSIS` (`Description`, `Appointments_AppointmentID`)" +
                    " VALUES (@desc, @appId)";


                using (var cmd = new MySqlCommand(sql))

                {
                    cmd.Parameters.AddWithValue("@desc", entity.DiagnosisDescription);
                    cmd.Parameters.AddWithValue("@appId", entity.AppointmentsAppointmentId);

                    cmd.Connection = this.connection;
                    cmd.Connection.Open();

                    cmd.ExecuteNonQuery();

                    cmd.Connection.Close();
                }
            }
        }

        /// <summary>
        ///     Gets the byld.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The Diagnosis by finding it's Id</returns>
        public Diagnosis GetByld(int id)
        {
            const string sql = "GetDiaoById( " + " @diagId " + " )";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@diagId", id);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        this.diagnosis = new Diagnosis(Convert.ToInt32(reader["DiagnosisID"]),
                            Convert.ToString(reader["Description"]),
                            Convert.ToInt32(reader["Appointments_AppointmentID"]),
                            Convert.ToString(reader["FINAL_DIAG"]));
                    }
                }

                cmd.Connection.Close();
            }
            return this.diagnosis;
        }

        /// <summary>
        ///     Gets all.
        /// </summary>
        /// <returns>A list of all Diagnosis</returns>
        public IList<Diagnosis> GetAll()
        {
            var listOfDiagnosis = new List<Diagnosis>();

            const string sql = "GetDiaoById()";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        listOfDiagnosis.Add(new Diagnosis(Convert.ToInt32(reader["DiagnosisID"]),
                            Convert.ToString(reader["Description"]),
                            Convert.ToInt32(reader["Appointments_AppointmentID"]),
                            Convert.ToString(reader["FINAL_DIAG"])));
                    }
                }

                cmd.Connection.Close();
            }
            return listOfDiagnosis;
        }

        internal void UpdateDioResult(int dioId, string resultString)
        {
            const string sql =
                "UPDATE DIAGNOSIS "
                + "SET FINAL_DIAG = @Final_Diag "
                + "WHERE DiagnosisID = @DiagnosisID ";

            using (var cmd = new MySqlCommand(sql))
            {
                cmd.Parameters.AddWithValue("@Final_Diag", resultString);
                cmd.Parameters.AddWithValue("@DiagnosisID", dioId);
                cmd.Connection = this.connection;

                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        cmd.Connection.Open();

                        cmd.ExecuteNonQuery();

                        cmd.Connection.Close();
                    }
                }

                cmd.Connection.Close();
            }
        }

        /// <summary>
        ///     Gets the diagnoses information for patient.
        /// </summary>
        /// <param name="thePatient">The patient.</param>
        /// <returns>Diagnosis information about a patient</returns>
        public List<Diagnosis> GetDiagnosesInformationForPatient(Patient thePatient)
        {
            {
                var listOfDiagnosis = new List<Diagnosis>();

                const string sql =
                    "SELECT * " +
                    " FROM PATIENT, APPOINTMENTS, DIAGNOSIS " +
                    " WHERE  APPOINTMENTS.Patient_PatientId = PATIENT.PatientId AND APPOINTMENTS.appointmentId = DIAGNOSIS.Appointments_AppointmentID AND PATIENT.PatientId = @id";

                using (var cmd = new MySqlCommand(sql))

                {
                    cmd.Parameters.AddWithValue("@id", thePatient.PatientId);

                    cmd.Connection = this.connection;
                    cmd.Connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())

                        {
                            listOfDiagnosis.Add(new Diagnosis(Convert.ToInt32(reader["DiagnosisID"]),
                            Convert.ToString(reader["Description"]),
                            Convert.ToInt32(reader["Appointments_AppointmentID"]),
                            Convert.ToString(reader["FINAL_DIAG"])));
                        }
                    }

                    cmd.Connection.Close();
                }
                return listOfDiagnosis;
            }
        }

        /// <summary>
        ///     Checks to see if dio exists.
        /// </summary>
        /// <param name="appointmentId">The appointment identifier.</param>
        /// <returns>whether a diagnosis already exists</returns>
        public bool CheckToSeeIfDioExists(int appointmentId)
        {
            const string sql =
                "SELECT * From DIAGNOSIS, APPOINTMENTS WHERE " +
                " Appointments_AppointmentID = AppointmentID AND AppointmentID = @aid ";

            var appointmentExist = false;

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@aid", appointmentId);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        appointmentExist = true;
                    }
                }

                cmd.Connection.Close();
            }
            return appointmentExist;
        }

        public Boolean CheckToSeeIfDioDoesntHaveAFinalDio(int dId)
        {
            const string sql =
                "SELECT * " +
                " From DIAGNOSIS " +
                " WHERE DiagnosisID = @dId AND FINAL_DIAG IS NULL ";


            var resultExist = false;

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@dId", dId);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        resultExist = true;
                    }
                }

                cmd.Connection.Close();
            }
            return resultExist;
        }

        #endregion
    }
}