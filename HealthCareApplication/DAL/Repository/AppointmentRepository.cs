﻿using System;
using System.Collections.Generic;
using HealthCareApplication.DAL.Interfaces;
using HealthCareApplication.Model;
using MySql.Data.MySqlClient;

namespace HealthCareApplication.DAL.Repository
{
    internal class Appointmentrepository : IRepository<Appointment>
    {
        #region Data members

        private readonly MySqlConnection connection;
        private Appointment targetAppointment;

        #endregion

        #region Constructors

        public Appointmentrepository(string connectionLabel = "MySqlDbConnection")
        {
            this.connection = DbConnection.GetConnection(connectionLabel);
        }

        #endregion

        #region Methods

        public void Add(Appointment entity)
        {
            {
                const string sql =
                    " INSERT INTO `APPOINTMENTS` (`Date/Time`, `Reason_for_appointment`, `Routine_checks_Routine_check_id`, `Doctors_DoctorID`, `Patient_PatientId`) " +
                    " VALUES (@time, @reason, @checkid, @doctorid, @patid)";

                using (var cmd = new MySqlCommand(sql))

                {
                    cmd.Parameters.AddWithValue("@time", entity.DateTime);
                    cmd.Parameters.AddWithValue("@reason", entity.ReasonForAppointment);
                    cmd.Parameters.AddWithValue("@checkid", entity.RoutineCheckId);
                    cmd.Parameters.AddWithValue("@doctorid", entity.DoctorId);
                    cmd.Parameters.AddWithValue("@patid", entity.PatientId);

                    cmd.Connection = this.connection;
                    cmd.Connection.Open();

                    cmd.ExecuteNonQuery();

                    cmd.Connection.Close();
                }
            }
        }

        public Appointment GetByld(int id)
        {
            const string sql = "SELECT * From APPOINTMENTS WHERE " + "AppointmentID = @appointmentId";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@appointmentId", id);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        this.targetAppointment = new Appointment(Convert.ToInt32(reader["AppointmentID"]),
                            Convert.ToDateTime(reader["Date/Time"]),
                            Convert.ToString(reader["Reason_for_appointment"]),
                            Convert.ToInt32(reader["Routine_checks_Routine_check_id"]),
                            Convert.ToInt32(reader["Doctors_DoctorID"]),
                            Convert.ToInt32(reader["Patient_PatientID"]));
                    }
                }

                cmd.Connection.Close();
            }
            return this.targetAppointment;
        }

        public IList<Appointment> GetAll()
        {
            var listOfAppointments = new List<Appointment>();

            const string sql = "SELECT * From APPOINTMENTS";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        listOfAppointments.Add(new Appointment(Convert.ToInt32(reader["AppointmentID"]),
                            Convert.ToDateTime(reader["Date/Time"]),
                            Convert.ToString(reader["Reason_for_appointment"]),
                            Convert.ToInt32(reader["Routine_checks_Routine_check_id"]),
                            Convert.ToInt32(reader["Doctors_DoctorID"]),
                            Convert.ToInt32(reader["Patient_PatientID"])));
                    }
                }

                cmd.Connection.Close();
            }
            return listOfAppointments;
        }

        internal List<Appointment> GetAppointmentsBetweenDates(DateTime startDate, DateTime endDate)
        {
            var listOfAppointments = new List<Appointment>();

            const string sql = "select * " +
                               " FROM APPOINTMENTS " +
                               " WHERE `Date/Time` >= @startDate and `Date/Time` <= @endDate";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@startDate", startDate);
                cmd.Parameters.AddWithValue("@endDate", endDate);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        listOfAppointments.Add(new Appointment(Convert.ToInt32(reader["AppointmentID"]),
                            Convert.ToDateTime(reader["Date/Time"]),
                            Convert.ToString(reader["Reason_for_appointment"]),
                            Convert.ToInt32(reader["Routine_checks_Routine_check_id"]),
                            Convert.ToInt32(reader["Doctors_DoctorID"]),
                            Convert.ToInt32(reader["Patient_PatientID"])));
                    }
                }

                cmd.Connection.Close();
            }
            return listOfAppointments;
        }

        public Appointment GetAppointmentByCheckId(int id)
        {
            const string sql =
                "SELECT * From APPOINTMENTS WHERE " +
                "Routine_checks_Routine_check_id = @Routine_checks_Routine_check_id";

            using (var cmd = new MySqlCommand(sql))

            {
                cmd.Parameters.AddWithValue("@Routine_checks_Routine_check_id", id);

                cmd.Connection = this.connection;
                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())

                    {
                        this.targetAppointment = new Appointment(Convert.ToInt32(reader["AppointmentID"]),
                            Convert.ToDateTime(reader["Date/Time"]),
                            Convert.ToString(reader["Reason_for_appointment"]),
                            Convert.ToInt32(reader["Routine_checks_Routine_check_id"]),
                            Convert.ToInt32(reader["Doctors_DoctorID"]),
                            Convert.ToInt32(reader["Patient_PatientID"]));
                    }
                }

                cmd.Connection.Close();
            }
            return this.targetAppointment;
        }

        public void SetTHeLastMadeCheckToAppointmentWithId(int appointmentid, int checkid)
        {
            const string sql =
                "UPDATE APPOINTMENTS "
                + "SET Routine_checks_Routine_check_id = @checkID "
                + "WHERE AppointmentID = @appointmentID ";

            using (var cmd = new MySqlCommand(sql))
            {
                cmd.Parameters.AddWithValue("@checkID", checkid);
                cmd.Parameters.AddWithValue("@appointmentID", appointmentid);
                cmd.Connection = this.connection;

                cmd.Connection.Open();

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        cmd.Connection.Open();

                        cmd.ExecuteNonQuery();

                        cmd.Connection.Close();
                    }
                }

                cmd.Connection.Close();
            }
        }

        public void UpdateAppoinmentWithACheckId(int appointmentId, int checkId)
        {
            {
                const string sql = "UPADE APPOINTMENTS "
                                   + "SET Routine_checks_Routine_check_id = @checkID "
                                   + "WHERE AppointmentID = @appointmentId ";

                using (var cmd = new MySqlCommand(sql))

                {
                    cmd.Parameters.AddWithValue("@appointmentId", appointmentId);
                    cmd.Parameters.AddWithValue("@checkID", checkId);

                    cmd.Connection = this.connection;

                    cmd.Connection.Open();

                    cmd.ExecuteNonQuery();

                    cmd.Connection.Close();
                }
            }
        }

        public List<Appointment> GetAppointInformationForPatient(Patient thePatient)
        {
            {
                var listOfAppointments = new List<Appointment>();

                const string sql =
                    "SELECT * " +
                    " FROM PATIENT, APPOINTMENTS " +
                    " WHERE  APPOINTMENTS.Patient_PatientId = PATIENT.PatientId AND PATIENT.PatientId = @id";

                using (var cmd = new MySqlCommand(sql))

                {
                    cmd.Parameters.AddWithValue("@id", thePatient.PatientId);

                    cmd.Connection = this.connection;
                    cmd.Connection.Open();

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())

                        {
                            listOfAppointments.Add(new Appointment(Convert.ToInt32(reader["AppointmentID"]),
                                Convert.ToDateTime(reader["Date/Time"]),
                                Convert.ToString(reader["Reason_for_appointment"]),
                                checkForNull("Routine_checks_Routine_check_id", reader),
                                Convert.ToInt32(reader["Doctors_DoctorID"]),
                                Convert.ToInt32(reader["Patient_PatientId"])));
                        }
                    }

                    cmd.Connection.Close();
                }
                return listOfAppointments;
            }
        }

        private static int? checkForNull(string o, MySqlDataReader reader)
        {
            if (reader.IsDBNull(reader.GetOrdinal(o)))
            {
                return int.MaxValue;
            }
            return Convert.ToInt32(reader[o]);
        }

        #endregion
    }
}