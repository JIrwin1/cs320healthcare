﻿using System.Configuration;
using MySql.Data.MySqlClient;

namespace HealthCareApplication.DAL
{
    /// <summary>
    ///     Creates connection to database
    /// </summary>
    public class DbConnection
    {
        #region Methods

        /// <summary>
        ///     Gets the connection information for sql connection.
        /// </summary>
        /// <param name="connlable">The connlable.</param>
        /// <returns></returns>
        public static MySqlConnection GetConnection(string connlable)
        {
            var item = ConfigurationManager.ConnectionStrings[connlable].ConnectionString;
            var connection = new MySqlConnection(item);

            return connection;
        }

        #endregion
    }
}